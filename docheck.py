#!/usr/bin/env python3

import os
import sys
import glob

if 'GOOGLETEST_HOME' not in os.environ:
    print ('GOOGLETEST_HOME not in environment!')
    sys.exit (1)

def getpacks():
    packs = {}
    for l in open('SConstruct').readlines():
        if l.find ('Defaultcc') >= 0:
            ipos0 = l.find("'")
            ipos1 = l.find("'", ipos0+1)
            if ipos0 >= 0 and ipos1 >= 0:
                path = l[ipos0+1:ipos1]
                pack = path.split('/')[-1]
                l = packs.setdefault(pack,[])
                if path not in l: l.append (path)
    return packs

def includes (packs):
    others = ' -I$GEOMODELCORE_HOME/GeoGenericFunctions -I$GEANT_HOME/include '
    return ' '.join (['-I'+p for p in sum(packs.values(), [])]) + others

def checkpack (path):
    print ('---> Checking ' + path, end='')
    cxx = glob.glob (path + '/*/*.cxx') + glob.glob (path + '/*/*/*.cxx')
    cpp = glob.glob (path + '/*/*.cpp')
    cc = glob.glob (path + '/*/*.cc')
    if not cxx+cpp+cc and path.find('ByteStreamCnvSvcLegacy') < 0:
        print (' ... skipped ')
        return
    print ('')
    sys.stdout.flush()
    #cppcheck='cppcheck'
    cppcheck='/home/sss/cppcheck/inst/bin/cppcheck'

    supp = '--suppress=useStlAlgorithm --suppress=assignmentInAssert:*_test.* --suppress=assignmentInAssert:*/test/* --suppress=assertWithSideEffect:*_test.* --suppress=assertWithSideEffect:*/test/* --suppress=accessMoved --suppress=uninitMemberVar --suppress=missingReturn:*gmock-*.h --suppress=virtualCallInConstructor:*gmock-*.h --suppress=returnTempReference:*gmock-*.h --suppress=preprocessorErrorDirective:*gtest-*.h --suppress=returnReference:*gtest-*.h --suppress=passedByValue:*gtest-*.h --suppress=mismatchingContainers:*gtest-*.h --suppress=*:*/Geant/* '
    # As of 2.12 we now get these warnings for functions as well, which
    # is just too much.
    supp = supp + ' --suppress=duplInheritedMember '

    cmd = '%s -q -j14 --std=c++20 --enable=warning,portability,performance  %s --inline-suppr --template=gcc -D__CPPCHECK__ -DATOMIC_POINTER_LOCK_FREE=2 -D__linux__ -D__GNUC__=13 --library=AthenaConfig.xml --check-level=exhaustive  %s %s' % (cppcheck, supp, includes(packs), path)
    #print(cmd, flush=True)
    os.system (cmd)
    return

packs = getpacks()
if len(sys.argv) < 2:
    for (pack, paths) in packs.items():
        for path in paths:
            checkpack (path)
else:
    for pack in sys.argv[1:]:
        for path in packs[pack]:
            checkpack (path)


