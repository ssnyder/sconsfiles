#!/usr/bin/env python

import fcntl
import os
import sys
import shelve
import dbm.gnu

#The confdb2_part files rely on GaudiHandles. Importing them here so we
#can interpret these files later on
from GaudiKernel.GaudiHandles import *
from GaudiKernel.DataHandle import DataHandle

def merge (dbfile, part):
    payload = open(part).read()
    payload = eval (payload)

    lockFile = open( dbfile + '.lock', 'a' )
    try:
        fcntl.lockf( lockFile, fcntl.LOCK_EX )
    except IOError as exc_value:
        print ("Problem when trying to lock {0}, IOError {1}".format(mergedFile, exc_value[0]))
        raise

    try:
        tmpfile = dbfile + '.tmp'
        if os.path.exists (tmpfile):
            os.remove (tmpfile)
        if os.path.exists (dbfile):
            os.system ('cp ' + dbfile + ' ' + tmpfile)
        db = shelve.Shelf (dbm.gnu.open (tmpfile, 'c'))
        db.update (payload)
        db.close()
        os.rename (tmpfile, dbfile)
    finally:
        # unlock file
        fcntl.lockf( lockFile, fcntl.LOCK_UN )

    return


if __name__ == "__main__":
    db = sys.argv[1]
    merge (db, sys.argv[2])
    
