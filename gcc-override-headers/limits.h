#ifndef LIMITS_WRAPPER
#define LIMITS_WRAPPER

#include_next <limits.h>

#undef ULONG_MAX
#define ULONG_MAX UINT_MAX

#endif
