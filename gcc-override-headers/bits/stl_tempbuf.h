#ifndef STL_TEMPBUF_WRAPPER
#define STL_TEMPBUF_WRAPPER

#define __has_trivial_constructor(T) true
#include_next <bits/stl_tempbuf.h>
#undef __has_trivial_constructor

#endif
