#ifndef GCCXML_CXXCONFIG_H
#define GCCXML_CXXCONFIG_H

#include_next <bits/c++config.h>


#ifndef _GLIBCXX_BEGIN_NESTED_NAMESPACE
# define _GLIBCXX_BEGIN_NESTED_NAMESPACE(X, Y) _GLIBCXX_BEGIN_NAMESPACE(X)
#endif

#ifndef _GLIBCXX_BEGIN_NAMESPACE
# define _GLIBCXX_BEGIN_NAMESPACE(X) namespace X _GLIBCXX_VISIBILITY_ATTR(default) {
#endif

#ifndef _GLIBCXX_VISIBILITY_ATTR
# define _GLIBCXX_VISIBILITY_ATTR(V) 
#endif


//#ifndef _GLIBCXX_VISIBILITY
#define _GLIBCXX_VISIBILITY(x)
//#endif


#define _GLIBCXX_BEGIN_NAMESPACE_CONTAINER
#define _GLIBCXX_END_NAMESPACE_CONTAINER

#define __is_trivial(x) false
#define __decltype typeof

#ifdef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_ATOMIC_BUILTINS
#endif

#ifdef _GLIBCXX_USE_INT128
#undef _GLIBCXX_USE_INT128
#endif


#endif

