#ifndef BYTESWAP_WRAPPER
#define BYTESWAP_WRAPPER

unsigned int __builtin_bswap32(unsigned int);
unsigned long long __builtin_bswap64(unsigned long long);

#include_next <bits/byteswap.h>

#endif
