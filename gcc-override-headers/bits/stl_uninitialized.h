#ifndef STL_UNINITIALIZED_WRAPPER
#define STL_UNINITIALIZED_WRAPPER

#define __has_trivial_constructor(T) true
#include_next <bits/stl_uninitialized.h>
#undef __has_trivial_constructor

#endif
