#ifndef CTYPE_WRAPPER
#define CTYPE_WRAPPER

unsigned int __builtin_bswap32(unsigned int);
unsigned long long __builtin_bswap64(unsigned long long);

#include_next <ctype.h>

#endif
