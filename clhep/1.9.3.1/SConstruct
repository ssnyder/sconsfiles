# This is -*- python -*-

SConscript("../../sconslib/lib")
Import(["env", "makeenv", "buildlib", "buildtests", "buildp"])
import os
import string
import glob

clhepenv = env.Clone()
clhepenv['CXXFILESUFFIX'] = '.cc'
clhepenv['SRCDIR'] = 'src/CLHEP/$PACKAGE/src'
clhepenv['INCDIR'] = 'src/CLHEP/$PACKAGE/$PACKAGE'
clhepenv['LIBNAME'] = 'CLHEP-${PACKAGE}'
clhepenv['CLIDDB_FLAG'] = False

def symlink_command (source, target, env, for_signature=None):
    l = os.path.dirname(target[0].path).split('/')
    #updir = string.join (['..']*len(l), '/')
    updir = os.getcwd()
    cmd = "ln -sf %s/%s %s" % (updir, source[0].path, target[0].path)
    return cmd
clhepenv.Append (BUILDERS = {'Symlink' :
                             Builder (generator = symlink_command)})

def makelinks (env):
    if not buildp(env): return
    incdir = env.subst ('$INCDIR')
    exts = ['.h', '.icc', '.hh']
    headers = []
    for e in exts:
        headers = headers + glob.glob(incdir + '/*' + e)
    targetdir = env.subst ('build/CLHEP/$PACKAGE')
    for h in headers:
        base = os.path.basename (h)
        Default (env.Symlink (targetdir + '/' + base, h))
    return


common_defs = """
#ifndef ${PACKAGE}_DEFS_H
#define ${PACKAGE}_DEFS_H
#ifndef ENABLE_BACKWARDS_COMPATIBILITY
#define ENABLE_BACKWARDS_COMPATIBILITY
#endif
@@
#ifndef PACKAGE
#define PACKAGE "$PACKAGE"
#endif
#ifndef PACKAGE_BUGREPORT
#define PACKAGE_BUGREPORT "/dev/null"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "$PACKAGE"
#endif
#ifndef PACKAGE_STRING
#define PACKAGE_STRING "$PACKAGE-1.9.4.2"
#endif
#ifndef PACKAGE_TARNAME
#define PACKAGE_TARNAME "$PACKAGE"
#endif
#undef PACKAGE_VERSION
#define PACKAGE_VERSION "1.9.4.2"
#undef VERSION
#define VERSION "1.9.4.2"
#endif
"""

def makedefs (env, defs=""):
    lines = common_defs.split ('\n')
    i = lines.index('@@')
    lines[i:i+1] = defs.split('\n')
    Default (env.Command ("build/CLHEP/$PACKAGE/defs.h",
                          None,
                          ["rm -f $TARGET"] +
                          ["echo '%s' >> $TARGET" % l
                           for l in lines]))
    return

clhepenv.Append (BUILDONLY = [
    'config',
    'Units',
    'Random', # CHANGED
    'Vector',
    'Geometry',
    'GenericFunctions',
    'Matrix',
    ])

##############################################################################

env_config = makeenv (clhepenv, 'config')
makedefs (env_config)
makelinks (env_config)

##############################################################################

env_units = makeenv (clhepenv, 'Units')
makedefs (env_units)
makelinks (env_units)

##############################################################################

env_random = makeenv (clhepenv, 'Random')
env_random.Append(CPPPATH = 'src/CLHEP/Random')
makelinks (env_random)
makedefs (env_random, "#define HAVE_DRAND48 1")
env_random['LIBS'].remove('-lCLHEP-Matrix')
env_random['LIBS'].remove('-lCLHEP-Vector')
env_random['LIBS'].remove('-lCLHEP-Random')
random_lib = buildlib (env_random)

##############################################################################

env_vector = makeenv (clhepenv, 'Vector')
env_vector.Append(CPPPATH = 'src/CLHEP/Vector')
makelinks (env_vector)
makedefs (env_vector)
env_vector['LIBS'].remove('-lCLHEP-Matrix')
env_vector['LIBS'].remove('-lCLHEP-Vector')
vector_lib = buildlib (env_vector)

##############################################################################

env_geometry = makeenv (clhepenv, 'Geometry')
env_geometry.Append(CPPPATH = 'src/CLHEP/Geometry')
makelinks (env_geometry)
makedefs (env_geometry)
env_geometry['LIBS'].remove('-lCLHEP-Matrix')
geometry_lib = buildlib (env_geometry)

##############################################################################
env_genericfunctions = makeenv (clhepenv, 'GenericFunctions')
env_genericfunctions.Append(CPPPATH = 'src/CLHEP/GenericFunctions')
makelinks (env_genericfunctions)
makedefs (env_genericfunctions)
env_genericfunctions['LIBS'].remove('-lCLHEP-Matrix')
genericfunctions_lib = buildlib (env_genericfunctions)

##############################################################################
env_matrix = makeenv (clhepenv, 'Matrix')
env_matrix.Append(CPPPATH = 'src/CLHEP/Matrix')
env_matrix['LIBS'].remove('-lCLHEP-Matrix')
makelinks (env_matrix)
makedefs (env_matrix)
matrix_lib = buildlib (env_matrix)

##############################################################################

