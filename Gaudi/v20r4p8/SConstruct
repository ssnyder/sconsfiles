# This is -*- python -*-

SConscript("../../sconslib/lib")
Import(["env", "makeenv", "buildlib", "buildtests", "builddict",
        "install_python", "buildbin", 'genenv', 'Default', 'top_env'])
import glob

top_env (env)
env['CXXFILESUFFIX'] = '.cpp'
env['CLIDDB_FLAG'] = False
env['PROJECT'] = 'Gaudi'

env.Append (BUILDONLY = [
    'GaudiKernel',  # CHANGED
    'GaudiSvc',  # CHANGED
    'GaudiUtils',
    'GaudiAud',
    'GaudiAlg',
    'GaudiPython', # CHANGED
    'PartPropSvc', # CHANGED
    'RootHistCnv',
    ])

for p in env['BUILDONLY']:
    Alias (p)
    Default (p)

##############################################################################
env_gaudikernel = makeenv (env, 'GaudiKernel',
        SRCDIR = 'GaudiKernel/src/Lib')
buildlib (env_gaudikernel)
builddict (env_gaudikernel,
           selfile = 'GaudiKernel/dict/dictionary.xml',
           dictfile = 'GaudiKernel/dict/dictionary.h')
install_python (env_gaudikernel, namelist='python/GaudiKernel/*.py')
buildbin (env_gaudikernel, 'src/Util/genconf.cpp',
          libs=['boost_program_options'])

##############################################################################

gaudisvcs = [#'AIDATupleSvc', # needs GaudiPI?
             'AlgContextSvc',
             'ApplicationMgr',
             'AuditorSvc',
             'ChronoStatSvc',
             'DataListenerSvc',
             'DataSvc',
             'DetectorDataSvc',
             'Dll',
             'EventSelector',
             'ExceptionSvc',
             'FastContainersSvc',
             'HistogramSvc',
             'HistorySvc',
             'IncidentSvc',
             'IssueLogger',
             'JobOptionsSvc',
             'MessageSvc',
             'NTupleSvc',
             'ParticlePropertySvc',
             'PersistencySvc',
             'RndmGenSvc',  # needs CLHEP
             'StagerSvc',
             'StatusCodeSvc',
             'THistSvc',
             'ToolSvc',
             ]
env_gaudisvc = makeenv (env, 'GaudiSvc', ['GaudiKernel'],
                        SRCDIR=['GaudiSvc/src/' + f for f in gaudisvcs])
buildlib (env_gaudisvc, rootmap=True, genconf=True)

##############################################################################
env_gaudiutils = makeenv (env, 'GaudiUtils',
                        ['GaudiKernel'],
                        SRCDIR = 'GaudiUtils/src/component')
buildlib (env_gaudiutils, libname = 'GaudiUtilsLib',
          srcs = glob.glob('GaudiUtils/src/Lib/*.cpp'))
buildlib (env_gaudiutils, libname = 'GaudiUtils')

##############################################################################
env_gaudiaud = makeenv (env, 'GaudiAud',
                        ['GaudiKernel'])
buildlib (env_gaudiaud, component = True, genconf = True)

##############################################################################
env_gaudialg = makeenv (env, 'GaudiAlg',
                        ['GaudiKernel',
                         'GaudiUtils'],
                        SRCDIR = 'GaudiAlg/src/components')
buildlib (env_gaudialg, libname = 'GaudiAlgLib',
          srcs = glob.glob('GaudiAlg/src/lib/*.cpp'),
          extra_libs = 'GaudiUtilsLib')
buildlib (env_gaudialg, libname = 'GaudiAlg',
          extra_libs = 'GaudiAlgLib',
          genconf = True,
          component = True)

##############################################################################
env_gaudipython = makeenv (env, 'GaudiPython',
                           ['GaudiKernel',
                            'GaudiAlg'],
                           SRCDIR = 'GaudiPython/src/Services')
buildlib (env_gaudipython, libname = 'GaudiPythonLib',
          srcs = glob.glob ('GaudiPython/src/Lib/*cpp'))
buildlib (env_gaudipython, libname = 'GaudiPython')
builddict (env_gaudipython,
           selfile = 'GaudiPython/dict/selection_kernel.xml',
           dictfile = 'GaudiPython/dict/kernel.h',
           extra_libs = 'GaudiAlgLib GaudiPythonLib')
install_python (env_gaudipython, namelist='python/GaudiPython/*.py')
install_python (env_gaudipython, dest = '$PYBASE')

##############################################################################
with genenv (env, 'PartPropSvc') as e:
    e.add_deps ('GaudiKernel')
    e.build_lib (component = True, extra_libs = 'HepPDT')
    e.install_jo()

##############################################################################
with genenv (env, 'RootHistCnv') as e:
    e.add_deps ('GaudiKernel')
    e.build_lib (component = True)
