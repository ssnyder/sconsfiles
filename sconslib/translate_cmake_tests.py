#!/usr/bin/env python3

import glob
import os
import string
from io import StringIO


from translate_cmake import CMakeReader


failures = []

def testit(reffile):
    infile = os.path.splitext (reffile)[0]
    root = os.path.basename (infile)
    print ('--->', root)
    r = CMakeReader (root, addcwd = False)
    scons = StringIO()
    r.translate (open (infile), scons)
    if r.err:
        print ('translation error:', r.err)
        failures.append (os.path.basename (infile))
        return
    out = scons.getvalue()
    ref = open(reffile).read()
    if out != ref:
        logfile = root + '.log'
        fout = open(logfile, 'w')
        fout.write (out)
        fout.close()
        print ('diff -u %s %s' % (reffile, logfile))
        os.system ('diff -u %s %s' % (reffile, logfile))
        failures.append (os.path.basename (infile))
    return
    

def main():
    tests = glob.glob ('cmaketests/*.ref')
    tests.sort()
    for r in tests:
        testit(r)
    if failures:
        print ('Failures: ', ' '.join (failures))
    return
    

main()
