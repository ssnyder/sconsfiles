import sys
import os
import string

root6 = True

def _maybequote (s):
    if s == None: return ''
    return "'" + s + "'"


class Litstring:
    def __init__ (self, s):
        self.s = s
    def __repr__ (self):
        return self.s

class Reqreader:
    def __init__ (self, args={}):
        self.private = False
        self.err = False
        self.deps = []
        self.private_deps = []
        self.install_jo = []
        self.install_python = []
        self.install_data = []
        self.install_share = []
        self.install_xml = [] 
        self.install_files = []
        self.libs = []
        self.bins = []
        self.extra_libs = []
        self.extra_test_libs = []
        self.dicts = []
        self.scripts = []
        self.unittests = []
        self.athena_tests = []
        self.python_tests = []
        self.root_headers = []
        self.packbase = None
        self.component = False
        self.detcommon_lib = False
        self.linked = False
        self.tpcnv = False
        self.macros = {'q' : ['"', True],
                       'package' : ['\\${PACKAGE}', True],
                       'ROOTSYS' : ['$ROOTSYS', True],
                       'ROOT_home' : ['$ROOTSYS', True],
                       'Boost_linkopts_program_options' : ['-lboost_program_options', True],
                       'HepMC_IO_linkopts' : ['-lHepMCfio', True],
                       }
        if root6:
            self.macros['Reflex_linkopts'] = ['', True]
        else:
            self.macros['Reflex_linkopts'] = ['-lReflex -lCint -lCore', True]
        self.poolcnv_args = []
        self.sercnvs = []
        self.cpppaths = []
        self.cppflags = []
        self.cxxflags = []
        self.public_cpppaths = []
        self.libpaths = []
        self.no_rootmap = False
        self.do_ctest = False
        self.args = args
        self.tags = {}
        self.tags['G496'] = 1

        if os.environ['ARCH'].find ('clang') >= 0:
            self.tags['clang'] = 1
        else:
            self.tags['gcc'] = 1
        return

    
    def read_line (self, f):
        out = None
        while True:
            l = f.readline()
            if len(l) == 0: return out

            # cmt allows stuff like
            #   start \
            #   # some comment
            #   this continues the line
            if out and l[0] == '#': continue
            
            pos = l.find ('#')
            if pos >= 0:
                if l.strip()[-1] == '\\':
                    l = l[:pos] + '\\'
                else:
                    l = l[:pos]
            l = l.strip()
            if out:
                out = out + l
            else:
                out = l
            if out and out[-1] == '\\':
                out = out[:-1]
                if out: out = out + ' '
            elif out:
                return out
        return
    

    def translate (self, f, fout):
        self.fout = fout
        while True:
            l = self.read_line (f)
            if l == None: break
            if not l: continue
            ll = l.split()
            fn = getattr (self, 'do_' + ll[0], None)
            if fn:
                fn (ll)
            else:
                print ("Unknown req line", l, file=sys.stderr)
                self.err = 1
        self.dump ()
        self.check_macros()
        return


    def check_macros (self):
        self._refmacro (self.packbase + '_pedantic_level')
        self._refmacro ('POOL_FileMgt_libset')
        self._refmacro ('POOL_Services_libset')
        self._refmacro ('POOL_Collections_libset')
        self._refmacro ('application_suffix')
        self._refmacro ('all_dependencies')
        
        ign = self.args.get('ignored_macros', '')
        if type(ign) == type(''): ign = ign.split()
        for k, v in self.macros.items():
            if not v[1] and not k in ign:
                print ('Unreferenced macro', k, v[0], file=sys.stderr)
                self.err = True
        return


    def _append_cpppath (self, path):
        if self.private:
            self.cpppaths.append (path)
        else:
            self.public_cpppaths.append (path)
        return


    def do_version (self, ll):
        return


   
    def do_pattern (self, ll):
        return


    def do_alias (self, ll):
        return


    def do_action (self, ll):
        if ll[1] == 'checkreq':
            return
        print ('Unknown action line:', ll)
        self.errflag = True
        return


    def do_private (self, ll):
        self.private = True
        return

    def do_end_private (self, ll):
        self.private = False
        return

    def do_end_public (self, ll):
        #self.private = False
        return

    def do_public (self, ll):
        self.private = False
        return

    def do_branches (self, ll):
        return

    def do_make_fragment (self, ll):
        return

    def do_package (self, ll):
        self.packbase = os.path.basename (ll[1])
        self.macros[self.packbase + '_root'] = ["\\${PACKAGE}", True]
        self.macros[self.packbase.upper() + 'ROOT'] = ["\\${PACKAGE}", True]
        return

    def do_manager (self, ll):
        return

    def do_author (self, ll):
        return

    def do_include_dirs (self, ll):
        for l in ll[1:]:
            l = l.strip()
            if l[0] == '"' and l[-1] == '"':
                l = l[1:-1]
            self._append_cpppath (self._expand_macros (l))
        return

    def do_ignore_pattern (self, ll):
        return

    def do_set (self, ll):
        return

    def do_setup_script (self, ll):
        return

    def do_document (self, ll):
        if ll[1] == 'codewizard': return
        if ll[1] == 'athenarun_launcher':
            return self.handle_athenarun_launcher(ll)
        print ('Unhandled document:', ll)
        self.err = True
        return


    # only handles python tests for now...
    def handle_athenarun_launcher (self, ll):
        aa = self.split_args(ll)
        if ('-group=check' not in ll or
            aa.get('athenarun_exe') != 'python' or
            not 'athenarun_opt' in aa):
            print ('Unhandled document:', ll)
            self.err = True
        name = aa.get('athenarun_opt')
        if name.startswith ('../'): name = name[3:]
        d = {'tests' : [name]}

        postsh = aa.get('athenarun_post')
        if postsh:
            if postsh[0] == "'" and postsh[-1] == "'":
                postsh = postsh[1:-1]
            postsh = self._expand_macros (postsh)
            pp = postsh.split()
            ipos = postsh.find('"')
            if ipos > 0:
                d['extrapatterns'] = postsh[ipos+1:-1]
            postsh = pp[0]
            if postsh.startswith ('../'):
                postsh = '${PACKAGE}' + postsh[2:]
            if postsh == 'post.sh':
                postsh = 'AtlasTest/TestTools/share/post.sh'
            d['postsh'] = postsh
            del aa['athenarun_post']
            
        self.python_tests.append (d)
        return

    def do_macro (self, ll):
        fl = self._macro_line (ll[2:])
        fl = self._handle_macro_tags (fl)
        if ll[1] in ['cppdebugflags',
                     'DOXYGEN_IMAGE_PATH',
                     'CODECHECKINGOUTPUT']: return
        #if ll[1].endswith ('_linkopts'): return
        if ll[1].startswith ('DOXYGEN_'): return
        if ll[1] == 'poolcnv_include_extras' and ll[2] == '"../"': return
        if ll[1] == 'whichGroup': return
        if ll[1].endswith ('_TestConfiguration'): return
        if ll[1] == 'AtlasFastJetContrib_linkopts':
            for x in fl.split():
                if x.startswith('-l'):
                    self.extra_libs.append (x[2:])
            return

        self.macros.setdefault (ll[1], ['', False])[0] = fl
        return


    def do_macro_append (self, ll):
        fl = self._macro_line (ll[2:])
        fl = self._handle_macro_tags (fl)
        if not fl: return
        if ll[1].startswith ('DOXYGEN_'): return
        if (ll[1] == self.packbase + '_cppflags' or ll[1] == 'use_cppflags'):
            fl = fl.replace ('-Wno-unused', '')
            fl = fl.replace ('-ftemplate-depth-99', '')
            if fl.find ('-fno-strict-aliasing') >= 0:
                fl = fl.replace ('-fno-strict-aliasing', '')
                self.cppflags.append ('-fno-strict-aliasing')
            if fl.strip() == '': return
        if ll[1].endswith ('_cppflags'):
            fl = fl.replace ('-DNDEBUG', '')
            if fl.strip() == '': return
        if ll[1] == 'ROOT_linkopts':
            fl = fl.replace ('"', '')
            for opt in fl.split():
                if opt.startswith ('-l'):
                    self.extra_libs.append (opt[2:])
            return
        if ll[1] == 'Boost_linkopts':
            return
        if ll[1] == 'AtlasFastJet_linkopts' and fl.strip().startswith('-l'):
            self.extra_libs.append (fl.strip()[2:])
            return
        if ll[1] == 'cppflags':
            fl = fl.replace ('-Wno-format', '')
            fl = fl.replace ('-Wno-deprecated', '')
            if fl.find ('-fopenmp') >= 0:
                fl = fl.replace ('-fopenmp', '')
                self.cxxflags.append ('-fopenmp')
            if fl.strip() == '': return
        if ll[1].endswith ('Dict_dependencies'):
            if fl.strip() == self.packbase: return
        if ll[1].endswith ('_dependencies'):
            if fl.strip() == 'install_includes': return

        if ll[1].endswith ('_linkopts'):
            ref = ll[1][:-9]
            for dd in self.deps + self.private_deps + [self.packbase]:
                if os.path.basename(dd) == ref:
                    fl = fl.replace ('"', '')
                    for opt in fl.split():
                        if opt.startswith ('-l'):
                            self.extra_libs.append (opt[2:])
                    return
                    

        m = self.macros.setdefault (ll[1], ['', False])
        m[0] = self._handle_macro_tags (m[0]) + fl
        return


    def do_macro_prepend (self, ll):
        fl = self._macro_line (ll[2:])
        m = self.macros.setdefault (ll[1], ['', False])
        m[0] = self._handle_macro_tags (fl) + self._handle_macro_tags (m[0])
        return


    def do_macro_remove (self, ll):
        fl = self._macro_line (ll[2:])
        fl = self._handle_macro_tags (fl)
        if ll[1] == 'componentshr_linkopts' and ll[2] == '"-Wl,-s"':
            return
        if ll[1] == 'SEAL_linkopts':
            return
        if ll[1] == 'cppflags' and not fl.strip():
            return
        print ('Unhandled macro_remove:', ll)
        self.err = True
        return
    

    def do_use (self, ll):
        l = self._expand_macros (' '.join (ll))
        ll = l.split()
        if self.private:
            deplist = self.private_deps
        else:
            deplist = self.deps

        if len(ll) < 3: return
        if len(ll) >= 4:
            path = ll[3]
        else:
            path = ''

        self.macros[ll[1] + '_root'] = [ll[1] + '_root', True]
        self.macros[ll[1].upper() + 'ROOT'] = [os.path.join (path, ll[1]), True]

        if ll[1] == 'CoraCool':
            self.extra_libs.append ('lcg_CoolApplication')
            # Note: fall through
            
        if ll[-1] == '-no_auto_imports': return

        if ll[1] == 'Asg_Test':
            return

        if ll[1] == 'GaudiInterface' or ll[1] == 'Gaudi':
            deplist.append ('GaudiKernel')
            self.extra_libs.append ('GaudiPluginService')
            return

        if ll[1] == 'DQM_Core':
            deplist.append ('dqm_core')
            self.extra_libs.append ('dqm_core_io')
            return

        if ll[1] == 'GaudiPython':
            return

        if ll[1] == 'uuid':
            self.extra_libs.append ('uuid')
            return

        if ll[1] == 'AtlasBoost':
            self.extra_libs.append ('boost_system')
            return

        if ll[1] == 'AtlasXercesC':
            self.extra_libs.append ('xerces-c')
            return

        if ll[1] == 'XercesC':
            self.extra_libs.append ('xerces-c')
            return

        if ll[1] == 'AtlasTBB':
            self.extra_libs.append ('tbb')
            return

        if ll[1] == 'AtlasPOOL':
            deplist.append ('Database/APR/CollectionBase')
            deplist.append ('Database/APR/POOLCore')
            deplist.append ('Database/APR/PersistencySvc')
            deplist.append ('Database/APR/StorageSvc')
            #deplist.append ('lcg_FileCatalog')
            #deplist.append ('lcg_CollectionBase')
            #deplist.append ('lcg_PersistencySvc')
            #deplist.append ('lcg_CoralKernel')
            #deplist.append ('lcg_DataSvc')
            return

        if ll[1] == 'AtlasCORAL':
            deplist.append ('lcg_CoralBase')
            self.extra_libs.append ('lcg_ConnectionService')
            self.extra_libs.append ('lcg_RelationalAccess')
            self.extra_libs.append ('uuid')
            return

        if ll[1] == 'AtlasCOOL':
            self.extra_libs.append ('lcg_CoolKernel')
            return

        if ll[1] == 'AtlasCLHEP':
            self.extra_libs.append ('CLHEP-Matrix')
            self.extra_libs.append ('CLHEP-Vector')
            self.extra_libs.append ('CLHEP-Random')
            self.extra_libs.append ('CLHEP-GenericFunctions')
            self.extra_libs.append ('CLHEP-Geometry')
            self.extra_libs.append ('CLHEP-Evaluator')
            return

        if ll[1] == 'AtlasHepMC':
            self.extra_libs.append ('HepMC')
            self.extra_libs.append ('HepPDT')
            self.extra_libs.append ('HepPID')
            return

        if ll[1] == 'AtlasHdf5':
            self.extra_libs.append ('hdf5_cpp')
            self.extra_libs.append ('hdf5')
            return

        if ll[1] == 'AtlasPython':
            self.extra_libs.append ('python2.7')
            return

        if ll[1] == 'AtlasLapack':
            self.extra_libs.append ('lapack')
            return

        if ll[1] == 'AtlasFastJet':
            self._append_cpppath ('$FASTJET_HOME/include')
            self.libpaths.append ('$FASTJET_HOME/lib')
            #self.extra_libs.append ('SISConePlugin')
            #self.extra_libs.append ('CDFConesPlugin')
            #self.extra_libs.append ('CMSIterativeConePlugin')
            self.extra_libs.append ('fastjetplugins')
            self.extra_libs.append ('fastjet')
            self.extra_libs.append ('fastjettools')
            self.extra_libs.append ('siscone')
            self.extra_libs.append ('siscone_spherical')
            self.extra_libs.append ('EnergyCorrelator')
            self.extra_libs.append ('Nsubjettiness')
            self.extra_libs.append ('VariableR')
            self.extra_libs.append ('RecursiveTools')
            return

        if ll[1] == 'Lhapdf':
            self._append_cpppath ('$LHAPDF_HOME/include')
            self.libpaths.append ('$LHAPDF_HOME/lib')
            self.extra_libs.append ('LHAPDF')
            return

        if ll[1] == 'HEPUtils':
            self._append_cpppath ('$HEPUTILS_HOME/include')

        if ll[1] == 'MCUtils':
            self._append_cpppath ('$MCUTILS_HOME/include')

        if ll[1] == 'yampl':
            self._append_cpppath ('$YAMPL_HOME/include')
            self.libpaths.append ('$YAMPL_HOME/lib')
            self.extra_libs.append ('yampl')
            return

        if ll[1] == 'AtlasGdb':
            self.extra_libs.append ('bfd')
            return

        if ll[1] == 'AtlasEigen':
            self._append_cpppath ('$EIGEN_HOME')
            return

        if ll[1] == 'DataCollection':
            deplist.append ('eformat')
            deplist.append ('ers')
            deplist.append ('EventStorage')
            return

        if ll[1] == 'HLTtdaqcommon':
            deplist.append ('hltinterface')
            return

        if ll[1] == 'TDAQCPolicy':
            deplist.append ('hltinterface')
            return

        if ll[1] in ['AtlasPolicy', 'DetCommonPolicy', 'TestPolicy',
                     'AtlasFortranPolicy', 'AtlasCxxPolicy']:
            return

        if ll[1] == 'AtlasGSL':
            self.extra_libs.append ('gsl')
            self.extra_libs.append ('gslcblas')
            return

        if ll[1] == 'RootHistCnv':
            self.extra_libs.append ('RootHistCnv')
            return

        if ll[1] == 'CTVMFT':
            self.libpaths.append ('$CTVMFT_HOME/build/libs')
            self.extra_libs.append ('CTVMFT')
            return

        if ll[1] == 'AtlasCERNLIB':
            self.libpaths.append ('$CERN_HOME/lib')
            self.extra_libs.append ('packlib')
            self.extra_libs.append ('mathlib')
            self.extra_libs.append ('kernlib')
            self.extra_libs.append ('$G77LIB')
            return

        if ll[1] == 'PartPropSvc':
            return

        if ll[1] == 'HepPDT':
            self.extra_libs.append ('HepPDT')
            self.extra_libs.append ('HepPID')
            return

        if ll[1] == 'ulxmlrpcpp':
            self._append_cpppath ('$ULXMLRPCPP_HOME/include')
            self.libpaths.append ('$ULXMLRPCPP_HOME/lib')
            self.extra_libs.append ('ulxmlrpcpp')
            return

        if ll[1] == 'AtlasDSFMT':
            self._append_cpppath ('$DSFMT_HOME/include')
            self.libpaths.append ('$DSFMT_HOME/build/libs')
            self.extra_libs.append ('dSFMT')
            self.cppflags.append ('-DDSFMT_MEXP=19937')
            return

        if ll[1] == 'AtlasGoogleTest':
            self._append_cpppath ('$GOOGLETEST_HOME/include')
            self.libpaths.append ('$GOOGLETEST_HOME/lib')
            self.extra_test_libs += ['gtest']
            return

        if ll[1] == 'Geant4':
            self._append_cpppath ('$GEANT_HOME/include')
            self.libpaths.append ('$GEANT_HOME/lib/Linux-g++')
            self.extra_libs += ['G4physicslists',
                                'G4materials',
                                'G4tracking',
                                'G4run',
                                'G4processes',
                                'G4event',
                                'G4particles',
                                'G4digits_hits',
                                'G4track',
                                'G4geometry',
                                'G4graphics_reps',
                                'G4intercoms',
                                'G4global']
            return


        if path in ['External', 'LCG_Interfaces']: return
        if path == '-no_auto_imports':
            path = ''
        pack = os.path.join (path, ll[1])
        deplist.append (pack)
        return


    def do_use_ifndef (self, ll):
        args = self.split_args (ll)
        pkg = args['pkg']
        basename = os.path.basename(pkg)
        return self.do_use (['use', basename, basename + '-*', os.path.dirname(pkg)])


    def do_use_if (self, ll):
        return
        #args = self.split_args ([None] + ll)
        #pkg = args['pkg']
        #basename = os.path.basename(pkg)
        #return self.do_use (['use', basename, basename + '-*', os.path.dirname(pkg)])


    def do_use_unless (self, ll):
        args = self.split_args ([None] + ll)
        pkg = args['pkg']
        root = args['root']
        return self.do_use (['use', pkg, pkg + '-*', root])


    def do_library (self, ll):
        if len(ll) == 2 and ll[1] == self.packbase:
            return
        if len(ll) < 3:
            print ('Unhandled line:', ll, file=sys.stderr)
            self.err = True
            return
        if ll[2] == '-no_prototypes':
            del ll[2]
        args = self._parse_filelist (ll[2:])
        aa = [a
              for a in args if a not in ['*.cxx',
                                         '../src/*.cxx',
                                         'components/*.cxx',
                                         '../src/components/*.cxx',
                                         ]]
        if not aa: args = []
        d = {}
        args2 = []
        for a in args:
            if a.startswith ('-suffix='):
                d['objextra'] = a[8:]
            elif a == '-no_share':
                d['static'] = True
            else:
                args2.append (a)
        if args2:
            d['srcs'] = args2
        if ll[1] != self.packbase:
            d['libname'] = ll[1]
        if self.tpcnv:
            ex = d.get('except_srcs', '')
            ex = ex + ' ' + self.packbase + '.cxx'
            d['except_srcs'] = ex
        if (self.args.get ('no_components', False) and
            not 'no_components' in d):
            d['no_components'] = True
        self.libs.append (d)
        return


    def do_application (self, ll):
        if len(ll)>=3 and ll[2] == '-no_prototypes':
            del ll[2]
        if ll[-1].startswith ('bindirname='):
            ll = ll[:-1]

        suffix = None
        if ll[-1].startswith ('application_suffix='):
            suffix = ll[-1][19:]
            suffix=suffix.strip()
            if len(suffix)>=2 and suffix[0] == '"' and suffix[-1] == '"':
                suffix = suffix[1:-1]
            ll = ll[:-1]
        
        d = {'name' : ll[1],
             'srcs' : self._parse_filelist(ll[2:])}
        if suffix != None:
            d['suffix'] = suffix
        self.bins.append (d)
        return


    def dopat_pool_utility (self, ll, extralibs = []):
        aa  = self.split_args (ll)
        if not 'name' in aa or len(aa) != 1:
            print ('Bad pool_utility', ll)
            self.err = True
            return
            
        name = aa['name']
        d = {'name' : name,
             'srcs' : 'utilities/' + name + '.cpp',
             'suffix' : '',
             'libs' : ['GaudiKernel',
                       'lcg_ConnectionService',
                       'lcg_RelationalAccess'] + extralibs}
        self.bins.append (d)
        return


    def dopat_collection_utility (self, ll):
        return self.dopat_pool_utility (ll, ['curl'])

    
    def dopat_pool_plugin_library (self, ll):
        d = {'libname': self.packbase,
             'srcs': '*.cpp',
             'rootmap': True,
            }
        self.libs.append (d)
        return
       


    def dopat_install_xmls (self, ll):
        aa  = self.split_args (ll)
        d = {}
        if 'extras' in aa:
            d['namelist'] = \
                          self._parse_filelist (aa.get('extras').split())
            del aa['extras']
        if len(aa) != 0:
            print ('Unhandled install_xmls [%s]' % self.packbase)
            print (ll)
        self.install_xml.append (d)
        return


    def dopat_generic_declare_for_link (self, ll):
        aa = self.split_args (ll)
        d = {}
        d['destdir'] = "e.Dir('$SHAREDIR')"
        if 'kind' in aa:
            if aa['kind'] == 'data' or aa['kind'] == 'calib':
                pass
            elif aa['kind'] == 'xmls':
                d['destdir'] = "e.Dir('$XMLDIR')"
            else:
                print ('Unhandled line1:', ll)
                self.err = True
            del aa['kind']
        if 'prefix' in aa:
            pref = self._expand_macros (aa['prefix'])
            d['destdir'] = Litstring ("e.Dir('$BUILDNAME/%s')" % pref)
            del aa['prefix']
        files = aa['files']
        del aa['files']
        if 'name' in aa:
            del aa['name']
        if aa:
            print ('Unhandled line2:', ll)
        files = self._parse_filelist (files)
        d['namelist'] = []
        for f in files:
            if f.startswith ('../'): f = f[3:]
            d['namelist'].append (f)
        d['defdir'] = 'share'
        self.install_files.append (d)
        return


    def dopat_declare_job_transforms (self, ll):
        aa = self.split_args (ll)
        if 'tfs' in aa:
            ll2 = ['apply_pattern', 'generic_declare_for_link',
                   "files='-s=../share %s'" % aa['tfs'],
                   'prefix=share/bin']
            self.dopat_generic_declare_for_link (ll2)

            ll2 = ['apply_pattern', 'declare_python_modules',
                   "files='-s=../share %s'" % aa['tfs']]
            self.dopat_declare_python_modules (ll2)

            del aa['tfs']
        
        if 'jo' in aa:
            ll2 = ['apply_pattern', 'declare_joboptions',
                   "files='%s'" % aa['jo']]
            self.dopat_declare_joboptions (ll2)
            del aa['jo']

        if aa:
            print ('Unhandled line2:', ll)

        return


    def dopat_declare_calib (self, ll):
        aa = self.split_args (ll)
        d = {}
        d['destdir'] = "$SHAREDIR/$PACKBASE"
        files = aa['files']
        del aa['files']
        if aa:
            print ('Unhandled line2:', ll)
        files = self._parse_filelist (files)
        d['namelist'] = []
        for f in files:
            if f.startswith ('../'): f = f[3:]
            d['namelist'].append (f)
        d['defdir'] = 'share'
        self.install_files.append (d)
        return


    def dopat_generate_componentslist (self, ll):
        aa = self.split_args(ll)
        for l in self.libs:
            if l.get ('libname', self.packbase) == aa['library']:
                l['component'] = True
                l['genconf'] = False
        return


    def dopat_cmake_add_command (self, ll):
        return
    def dopat_cmake_add_libraries (self, ll):
        return
    def dopat_cmake_add_generated_files (self, ll):
        return
    def dopat_cmake_conditional_target (self, ll):
        return
    def dopat_cmake_override_library_type (self, ll):
        return
    def dopat_cmake_add_definitions (self, ll):
        return
    def dopat_cmake_set_macro (self, ll):
        return
    def dopat_cmake_add_dependency (self, ll):
        return
    def dopat_root5only_pattern (self, ll):
        return
    def dopat_optdebug_library (self, ll):
        return
    

    def _parse_filelist (self, ll, nomacros = False):
        args = ll
        if type(args) != type(''):
            args = ' '.join (args)
        args = args.replace ('"', '')
        args = args.replace ("'", '')
        args = args.split()
        out = []
        dir = None
        for a in args:
            if a.startswith ('-s='):
                dir = a[3:]
                if not nomacros:
                    dir = self._expand_macros (dir)
                epos = dir.find ('_root/')
                if epos > 0:
                    dir = dir[epos+6:]
                
            else:
                if not nomacros:
                    a = self._expand_macros(a)
                for aa in a.split():
                    if dir:
                        aa = os.path.join (dir, aa)
                    out.append (aa)
        return out


    def _handle_macro_tags (self, out):
        if type(out) == type(''):
            return out
        for (t, v) in out[1:]:
            if t in self.tags:
                return v
        return out[0][1]
    
    def _find_macro (self, mname):
        exp = self.macros.get (mname)
        if exp == None:
            return None
        exp[1] = True
        #return exp[0]

        return self._handle_macro_tags (exp[0])
        
    def _expand_macros1 (self, ss, beg, end):
        ipos = ss.find (beg)
        while ipos >= 0:
            if ipos > 0 and ss[ipos-1] == '\\':
                ss = ss[:ipos-1] + ss[ipos:]
                ipos = ss.find (beg, ipos)
                continue
            epos = ss.find (end, ipos)
            if epos > ipos:
                mname = ss[ipos+len(beg):epos]
                if mname in ['PACKAGE']:
                    ipos = ss.find (beg, ipos+1)
                    continue
                exp = self.macros.get (mname, ['', True])
                exp[1] = True
                out = self._handle_macro_tags (exp[0])
                ss = ss[:ipos] + out + ss[epos+len(end):]
            #ipos = ss.find (beg, ipos+len(beg))
            ipos = ss.find (beg, ipos)
        return ss
    def _expand_macros (self, ss):
        ss = self._expand_macros1 (ss, '$(', ')')
        ss = self._expand_macros1 (ss, '${', '}')
        return ss


    def _refmacro (self, m, v=[]):
        if type(v) != type([]):
            v = [v]
        mval = self.macros.get (m)
        if mval:
            if v and not mval[0] in v: return
            mval[1] = True
        else:
            self.macros[m] = ['', True]
        return


    # Returns list of (TAG, VALUE) pairs.
    def _macro_line1 (self, ll):
        args = ll
        if type(args) != type(''):
            args = ' '.join (['_default'] + args)
        args = args.strip()
        out = []
        while args:
            pos = args.find (' ')
            if pos < 0: break
            tag = args[:pos]
            args = args[pos:].strip()
            if args[0] == '"' or args[0] == "'":
                pos = args.find (args[0], 1)
                if pos < 0: break
                val = args[1:pos]
                args = args[pos+1:].strip()
            else:
                pos = args.find (' ')
                if pos < 0:
                    val = args
                    args = ''
                else:
                    val = args[:pos]
                    args = args[pos:].strip()
            val = self._expand_macros (val)
            out.append ((tag, val))
        return out


    def _macro_line (self, ll):
        if not ll: return ''
        out = self._macro_line1 (ll)
        return out
        #for (t, v) in out[1:]:
        #    if self.tags.has_key (t):
        #        return v
        #return out[0][1]


    def do_apply_pattern (self, ll):
        if len(ll) < 2: return
        patname = self._expand_macros (ll[1])
        fn = getattr (self, 'dopat_' + patname, None)
        if fn:
            fn (ll)
        else:
            print ('Unhandled pattern:', patname, file=sys.stderr)
            self.err = True
        return


    def do_include_path (self, ll):
        if len(ll) == 2 and ll[1] == 'none': return
        print ('Unhandled line', ll, file=sys.stderr)
        self.err = True
        return


    def do_apply_tag (self, ll):
        tag = self._expand_macros (ll[1])
        self.tags[tag] = 1
        if tag in ['ROOTSTLDictLibs',
                   'ROOTTableLibs',
                   'use_float_pers_version',
                   'UseAnalysisCamEvent',
                   ]: return
        if tag == 'NEEDS_CORAL_BASE':
            self.extra_libs.append ('lcg_CoralBase')
            self.extra_libs.append ('lcg_CoralKernel')
            return
        if tag == 'ROOTMathLibs' or ll[1] == 'rootMathLibs':
            self.extra_libs.append ('MathMore')
            self.extra_libs.append ('Minuit')
            self.extra_libs.append ('Minuit2')
            self.extra_libs.append ('Matrix')
            self.extra_libs.append ('Physics')
            self.do_apply_tag (['apply_tag', 'ROOTBasicLibs']) #ugly hack
            return
        if tag == 'ROOTBasicLibs':
            self.extra_libs.append ('Physics')
            ll = self._expand_macros ('$(rootBasicLibs)')
            for l in ll.split():
                if l.startswith ('-l'):
                    self.extra_libs.append (l[2:])
            return
        if tag == 'ROOTPhysicsLibs':
            self.extra_libs.append ('Physics')
            return
        if tag == 'ROOTCintexLibs':
            if not root6:
                self.extra_libs.append ('Cintex')
            return
        if tag == 'ROOTRooFitLibs':
            self.extra_libs.append ('RooFit')
            return
        if tag == 'ROOTTMVALibs':
            self.extra_libs.append ('TMVA')
            return
        if tag == 'ROOTEGLibs':
            self.extra_libs.append ('EG')
            return
        if tag == 'ROOTNetLibs':
            self.extra_libs.append ('Net')
            return
        if tag == 'ROOTGraphicsLibs':
            #self.extra_libs.append ('Graf')
            #self.extra_libs.append ('Graf3d')
            self.extra_libs.append ('Gpad')
            #self.extra_libs.append ('Html')
            #self.extra_libs.append ('Postscript')
            #self.extra_libs.append ('Gui')
            #self.extra_libs.append ('GX11TTF')
            #self.extra_libs.append ('GX11')
            return
        if tag == 'ROOTTMVA':
            self.extra_libs.append ('TMVA')
            return
        if tag == 'ROOTTreePlayer':
            self.extra_libs.append ('TreePlayer')
            return
        if tag == 'ROOTXMLIO':
            self.extra_libs.append ('XMLIO')
            return
        if tag == 'ROOTGenVectorLibs':
            self.extra_libs.append ('GenVector')
            return
        if tag == 'ROOTThreadLibs':
            self.extra_libs.append ('Thread')
            return
        if tag == 'ROOTExtraLibs':
            ll = self._expand_macros ('$(rootExtraLibs)')
            for l in ll.split():
                if l.startswith ('-l'):
                    self.extra_libs.append (l[2:])
            return
        if tag == 'NEEDS_COOL_FACTORY':
            self.extra_libs.append ('lcg_CoolKernel')
            self.extra_libs.append ('lcg_CoolApplication')
            return
        if tag == 'NEEDS_CORAL_RELATIONAL_ACCESS':
            return
        if tag == 'no_rootmap':
            for d in self.dicts:
                d['rootmap'] = False
            self.no_rootmap = True
            return
        if tag == 'no_merge_componentslist':
            for d in self.dicts:
                d['rootmap'] = False
            self.no_rootmap = True
            return
        if tag == '_noAllowUndefined': return
        if tag == 'notAsNeeded': return
        print ('Unhandled line:', ll)
        print ('Tag:', tag)
        self.err = True
        return


    def do_apply_tag_ifAnalysisBase (self, ll):
        return


    def _poolcnvns (self, f, ns, mult):
        fbase = os.path.splitext (os.path.split (f)[-1])[0]
        ismult = (fbase in mult.split())
        for n in ns.split():
            if n.endswith ('::' + fbase):
                ismult = ismult or (n in mult.split())
                if ismult: n = '*' + n
                return (f, n)
        if ismult:
            return (f, '*' + fbase)
        return f
    def dopat_poolcnv (self, ll):
        self.deps.append ('Database/AthenaPOOL/AthenaPoolCnvSvc')
        aa = self.split_args (ll)
        if aa == None:
            return

        # Defer this to get proper macro processing.
        self.poolcnv_args.append (aa)

        self._refmacro (self.packbase + 'PoolCnvGen_dependencies')
        self._refmacro (self.packbase + 'PoolCnv_dependencies')
        self._refmacro (self.packbase + 'PoolCnv_shlibflags',
                        ['$(%s_linkopts)' % self.packbase,
                         '$(%s_dict_linkopts)' % self.packbase])
        self._refmacro (self.packbase + '_poolIoHdlrTypes')
        return
    def process_poolcnv (self, aa):
        ns = ''
        if 'typesWithNamespace' in aa:
            ns = self._expand_macros (aa['typesWithNamespace'])
            del aa['typesWithNamespace']
        mult = ''
        if 'multChanTypes' in aa:
            mult = aa['multChanTypes']
            mult = self._expand_macros (mult)
            del aa['multChanTypes']
        if len(aa) != 1 or not 'files' in aa:
            print ('Unhandled poolcnv pattern', aa)
            self.err = True
            return
        ff = aa['files']

        ff = self._expand_macros (ff)

        fl = self._parse_filelist (ff, nomacros = True)
        fl = [self._poolcnvns(f, ns, mult) for f in fl]

        return fl


    def dopat_sercnv (self, ll):
        self.deps.append ('Trigger/TrigDataAccess/TrigSerializeCnvSvc')
        aa = self.split_args (ll)
        if aa == None:
            return
        d = {}
        ns = ''
        if 'typesWithNamespace' in aa:
            ns = self._expand_macros (aa['typesWithNamespace'])
            del aa['typesWithNamespace']

        if 'libtag' in aa:
            d['libtag'] = aa['libtag']
            del aa['libtag']
        if len(aa) != 1 or not 'files' in aa:
            print ('Unhandled sercnv pattern', ll)
            self.err = True
            return
        ff = aa['files']

        ff = self._expand_macros (ff)

        fl = self._parse_filelist (ff)
        fl = [self._poolcnvns(f, ns, '') for f in fl]

        d['names'] = fl
        self.sercnvs.append (d)

        return

    
    def do_dual_use_library (self, args, libname):
        args = self._parse_filelist (args)
        try:
            args.remove('*.icc')
        except ValueError:
            pass
        try:
            args.remove('*')
        except ValueError:
            pass
        aa = [a
              for a in args if a not in ['*.cxx',
                                         '../src/*.cxx',
                                         'components/*.cxx',
                                         '../src/components/*.cxx',
                                         ]]
        if not aa: args = []
        d = {'dual_use': True}
        if args:
            d['srcs'] = args
        if libname:
            d['libname'] = libname
        self.libs.append (d)
        return


    def dopat_dual_use_library (self, ll):
        aa = self.split_args (ll)
        if aa == None:
            return
        if len(aa) != 1 or not 'files' in aa:
            print ('Unhandled dual_use_library pattern', ll)
            self.err = True
            return
        return self.do_dual_use_library (aa['files'], None)


    def dopat_named_dual_use_library (self, ll):
        aa = self.split_args (ll)
        if aa == None:
            return
        if 'library' in aa:
            libname = aa['library']
            del aa['library']
        else:
            print ('Unhandled named_dual_use_library pattern', ll)
        if len(aa) != 1 or not 'files' in aa:
            print ('Unhandled named_dual_use_library pattern', ll)
            self.err = True
            return
        return self.do_dual_use_library (aa['files'], libname)


    def dopat_declare_joboptions (self, ll):
        aa = self.split_args (ll)
        if aa == None:
            return
        ff = aa.get('files')
        #if ff.strip() == '*.py':
        #    ff = None
        if aa.get('files') == None or len(aa) != 1:
            print ('Unhandled declare_joboptions pattern', ll)
            self.err = True
            return
        if ff:
            ff = ' '.join (self._parse_filelist (ff.split()))
        self.install_jo.append (ff)
        return


    def dopat_declare_python_submodule (self, ll):
        aa = self.split_args (ll)
        if aa == None:
            return
        submodule = aa.get('submodule')
        ff = aa.get('files')
        if ff.strip() == '*.py':
            ff = None
        if aa.get('files') == None or len(aa) != 2:
            print ('Unhandled declare_python_submodule pattern', ll)
            self.err = True
            return
        if ff:
            ff = ' '.join ([submodule + '/' + x for x in self._parse_filelist (ff.split())])
        else:
            ff = submodule + '/*.py'
        self.install_python.append ({'namelist' : ff,
                                     'dest' : '$PYDIR/' + submodule})
        return


    def dopat_declare_python_modules (self, ll):
        aa = self.split_args (ll)
        if aa == None:
            return
        ff = aa.get('files')
        if ff.strip() == '*.py':
            ff = None
        if aa.get('files') == None or len(aa) != 1:
            print ('Unhandled declare_python pattern', ll)
            self.err = True
            return
        if ff:
            ff = ' '.join (self._parse_filelist (ff.split()))
        self.install_python.append ({'namelist' : ff})
        return


    def dopat_declare_jobtransforms (self, ll):
        aa = self.split_args (ll)
        if aa == None:
            return
        ff = aa.get('trfs')
        if ff:
            del aa['trfs']
            if ff[0] == "'" and ff[-1] == "'": ff = ff[1:-1]
            ff = ' '.join (['scripts/'+f for f in self._parse_filelist (ff.split())])
            self.install_python.append ({'namelist' : ff})
        ff = aa.get('jo')
        if ff:
            del aa['jo']
            if ff[0] == "'" and ff[-1] == "'": ff = ff[1:-1]
            ff = ' '.join (self._parse_filelist (ff.split()))
            self.install_jo.append (ff)
        if aa:
            print ('Unhandled declare_jobtransforms pattern', ll)
            self.err = True
        return
            


    def dopat_declare_xmls (self, ll):
        return self.dopat_install_xmls (ll)


    def dopat_static_use (self, ll):
        return

    def dopat_validate_xml (self, ll):
        return

    def dopat_static_athena_app (self, ll):
        return

    def dopat_declare_docs (self, ll):
        return

    def dopat_declare_html (self, ll):
        return


    def dopat_ctest (self, ll):
        if len(ll) > 2:
            print ('Unhandled ctest pattern:', ll)
            self.err = True
            return
        self.do_ctest = True
        return


    def dopat_install_python_init (self, ll):
        if ll[2:]:
            print ('Unhandled install_python_init pattern', ll)
            self.err = True
            return
        return self.dopat_declare_python_modules ([None, None, 'files=*.py'])


    def dopat_declare_runtime (self, ll):
        aa = self.split_args (ll)
        if not aa: return
        fl = []
        if 'files' in aa:
            fl += self._parse_filelist (aa.get('files').split())
            del aa['files']
        if 'extras' in aa:
            fl += self._parse_filelist (aa.get('extras').split())
            del aa['extras']
        if len(aa) != 0:
            print ('Unhandled declare_runtime pattern', ll)
            self.err = True
            return
        self.install_share.append (' '.join (fl))
        return


    def do_declare_runtime_extras (self, ll):
        return self.dopat_declare_runtime_extras([None] + ll)
    def dopat_declare_runtime_extras (self, ll):
        aa = self.split_args (ll)
        if not 'extras' in aa or len(aa) != 1:
            print ('Unhandled declare_runtime pattern', ll)
            self.err = True
            return
        files = aa.get('extras')
        fl = self._parse_filelist (files.split())
        self.install_share.append (' '.join (fl))
        return


    def dopat_component_library (self, ll):
        self.component = True
        #self.libs[-1]['component'] = True
        return


    def dopat_component_library_no_genCLIDDB (self, ll):
        self.component = True
        self.libs[-1]['cliddb'] = False
        return


    def dopat_named_component_library (self, ll):
        aa = self.split_args(ll)
        for l in self.libs:
            if l.get ('libname', self.packbase) == aa['library']:
                l['component'] = True
        return


    def dopat_linked_library (self, ll):
        self.linked = True
        return


    def dopat_named_linked_library (self, ll):
        aa = self.split_args(ll)
        for l in self.libs:
            if l.get ('libname', self.packbase) == aa['library']:
                l['linked'] = True
        return


    def dopat_installed_library (self, ll):
        for l in self.libs:
            if not 'libname' in l:
                l['cliddb'] = False
                l['no_components'] = True
        return


    def dopat_named_installed_library (self, ll):
        aa = self.split_args (ll)
        for l in self.libs:
            if l.get ('libname', self.packbase) == aa['library']:
                l['cliddb'] = False
                l['default'] = True
        return


    def dopat_installed_linkopts (self, ll):
        return


    def dopat_no_include_path (self, ll):
        return


    def dopat_get_files (self, ll):
        # TODO!
        return


    def dopat_default_installed_library (self, ll):
        self.libs.append ({'srcs' : '*.cxx'})
        return


    def dopat_declare_scripts (self, ll):
        aa = self.split_args (ll)
        files = aa.get('files')
        fl = self._parse_filelist (files.split())
        self.scripts.append (' '.join (fl))
        return


    def dopat_do_genconf (self, ll):
        return


    def dopat_tpcnv_library (self, ll):
        self.tpcnv = True
        self.libs.append ({'libname' : self.packbase + 'Load',
                           'srcs' : self.packbase + '.cxx',
                           'libs' : self.packbase,
                           'rootmap' : True})
        dd = [l for l in self.libs
              if not 'libname' in l or l['libname'] == self.packbase]
        if len(dd) > 0:
            d = dd[0]
            ex = d.get('except_srcs', '')
            ex = ex + ' ' + self.packbase + '.cxx'
            d['except_srcs'] = ex
        return


    def dopat_named_tpcnv_library (self, ll):
        # Why was tpcnv_library making a separate Load library?
        aa = self.split_args(ll)
        for l in self.libs:
            if l.get ('libname', self.packbase) == aa['library']:
                l['component'] = True
                l['genconf'] = False
        return


    def dopat_lcgdict (self, ll):
        d = {}
        aa = self.split_args (ll)
        selfile = aa.get ('selectionfile')
        if selfile != 'selection.xml':
            if not selfile.endswith ('.xml'):
                print ('Unhandled lcgdict pattern1', ll)
                self.err = True
                return
            if selfile.find('/') != -1:
                selfile = '$PACKAGE/$PACKBASE/' + selfile
            d['selfile'] = selfile
        del aa['selectionfile']
        dictname = self.packbase
        header = aa.get ('headerfiles')
        header = header.replace ('\\', '')
        header = header.replace ('-s=${%s_root}/%s ' % (self.packbase,self.packbase),
                                 '../%s/' % self.packbase)
        if header != '../%s/%sDict.h' % (self.packbase, self.packbase):
            #pos = header.rfind ('Dict')
            #if pos < 0:
            #    print 'Unhandled lcgdict pattern3', ll
            #    self.err = True
            #    return
            if not header.endswith ('.h'):
                print ('Unhandled lcgdict pattern3a', ll)
                self.err = True
                return
            if header.startswith ('../'):
                header = '$PACKAGE' + header [2:]
            pat = '-s=$(' + self.packbase + '_root)/' + self.packbase
            if header.startswith (pat):
                header = header.split()[1]
            d['dictfile']  = header
        del aa['headerfiles']
        d['dictname'] = aa.get('dict')
        if not d['dictname'].endswith ('Dict'):
            d['dictname'] += 'Dict'
        del aa['dict']
        if aa.get('dataLinks'):
            dl = aa['dataLinks']
            del aa['dataLinks']
            d['dataLinks'] = self._expand_macros (dl).split()
        if aa.get('elementLinks'):
            dl = aa['elementLinks']
            del aa['elementLinks']
            d['elementLinks'] = self._expand_macros (dl).split()
        if aa.get('elementLinkVectors'):
            dl = aa['elementLinkVectors']
            del aa['elementLinkVectors']
            d['elementLinkVectors'] = self._expand_macros (dl).split()
        if aa.get('navigables'):
            dl = aa['navigables']
            del aa['navigables']
            d['navigables'] = self._expand_macros (dl).split()
        if aa.get('options'):
            d['parse_flags'] = self._expand_macros (aa['options'])
            del aa['options']
        if aa.get('extralibfiles'):
            f = self._expand_macros (aa['extralibfiles'])
            d['extra_srcs'] = [f]
            del aa['extralibfiles']
        if aa.get('extraselection'):
            f = self._expand_macros (aa['extraselection'])
            d['extraselection'] = [f]
            del aa['extraselection']
        if aa:
            print ('Unhandled lcgdict pattern4', aa, ll)
            self.err = True
            return
        if self.no_rootmap:
            d['rootmap'] = False
        self.dicts.append (d)
        return


    def dopat_declare_non_standard_include (self, ll):
        aa = self.split_args (ll)
        if len(aa) == 1 and aa.get('name') == 'doc':
            return
        print ('Unhandled declare_non_standard_include', ll)
        self.err= True
        return


    def dopat_UnitTest_run (self, ll):
        aa = self.split_args (ll)
        name = aa.get ('unit_test')
        if not name:
            print ('Unhandled UnitTest_run 1', ll)
            self.err = True
            return
        del aa['unit_test']
        if not name.endswith('_test'):
            name = name + '_test'
        d = {'tests' : [name]}
        extrapat = aa.get('extrapatterns')
        if extrapat:
            d['extrapatterns'] = self._expand_macros (extrapat)
            del aa['extrapatterns']
        extrasrc = aa.get('extra_sources')
        if extrasrc:
            d['extra_sources'] = self._expand_macros (extrasrc)
            del aa['extra_sources']
        if len(aa) > 0:
            print ('Unhandled UnitTest_run 2', ll)
            self.err = True
            return
        self.unittests.append (d)
        return


    def dopat_RelationalCollection_test_run (self, ll):
        # xxx TODO
        return
    

    def dopat_APR_test (self, ll):
        aa = self.split_args (ll)
        name = aa.get ('name')
        if not name:
            print ('Unhandled APR_test', ll)
            self.err = True
            return
        del aa['name']
        d = {'tests' : [name]}
        extrapat = aa.get('extrapatterns')
        if extrapat:
            d['extrapatterns'] = self._expand_macros (extrapat)
            del aa['extrapatterns']
        if len(aa) > 0:
            print ('Unhandled APR_test', ll)
            self.err = True
            return
        #self.unittests.append (d)
        # xxx TODO! --- needs cppunit
        return


    def dopat_athenarun_test (self, ll):
        aa = self.split_args (ll)
        name = aa.get ('name')
        if not name:
            print ('Unhandled athenarun_test', ll)
            self.err = True
            return
        del aa['name']

        options = aa.get ('options')
        if not options:
            print ('Unhandled athenarun_test', ll)
            self.err = True
            return
        del aa['options']
        d = {'jolist' : [options]}

        extrapat = aa.get('extrapatterns')
        if extrapat:
            d['extrapatterns'] = self._expand_macros (extrapat)
            del aa['extrapatterns']

        presh = aa.get('pre_script')
        if presh:
            if presh != '../cmt/setup.sh':
                #print 'Unhandled athenarun_test', ll
                #self.err = True
                #return
                pass
            del aa['pre_script']

        postsh = aa.get('post_script')
        if postsh:
            postsh = self._expand_macros (postsh)
            pp = postsh.split()
            ipos = postsh.find('"')
            if ipos > 0:
                d['extrapatterns'] = postsh[ipos+1:-1]
            postsh = pp[0]
            if postsh == 'post.sh':
                postsh = 'AtlasTest/TestTools/share/post.sh'
            d['postsh'] = postsh
            del aa['post_script']

        if len(aa) > 0:
            print ('Unhandled athenarun_test', ll)
            self.err = True
            return

        self.athena_tests.append (d)
        return


    def dopat_detcommon_ignores (self, ll):
        return


    def dopat_detcommon_header_installer (self, ll):
        return


    def dopat_detcommon_shared_library (self, ll):
        # Need to build dict???
        self.libs.append ({'cliddb' : False})
        return


    def dopat_detcommon_shared_generic_library (self, ll):
        aa = self.split_args (ll)
        if aa == None:
            return
        if len(aa) != 1 or not 'files' in aa:
            print ('Unhandled dual_use_library pattern', ll)
            self.err = True
            return
        args = self._parse_filelist (aa['files'])
        try:
            args.remove('*.icc')
        except ValueError:
            pass
        try:
            args.remove('*')
        except ValueError:
            pass
        aa = [a
              for a in args if a not in ['*.cxx',
                                         '../src/*.cxx',
                                         'components/*.cxx',
                                         '../src/components/*.cxx',
                                         ]]
        if not aa: args = []
        d = {'cliddb': False}
        if args:
            d['srcs'] = args
        self.libs.append (d)
        return


    def dopat_trigconf_application (self, ll):
        aa = self.split_args (ll)
        name = aa['name']
        del aa['name']
        if aa:
            print ('Unhandled trigconf_application', ll)
            self.err = True
        d = {'name' : 'TrigConf' + name,
             'srcs' : 'src/test/' + name + '.cxx',
             'suffix' : ''}
        self.bins.append (d)
        return


    def dopat_detcommon_link_files (self, ll):
        aa = self.split_args(ll)
        if not aa: return
        if 'kind' in aa:
            del aa['kind']
        if 'name' in aa:
            del aa['name']
        if not 'files' in aa:
            print ('Missing file list', ll)
            self.err = True
            return
        if not 'prefix' in aa:
            print ('Missing prefix', ll)
            self.err = True
            return
        if len(aa) != 2:
            print ('Bad detcommon_link_files', ll)
            self.err = True
            return
        files = self._parse_filelist (aa.get('files').split())
        d = {}
        d['destdir'] = Litstring ("e.Dir('$BUILDNAME/%s')" % aa['prefix'])
        d['namelist'] = []
        for f in files:
            if f.startswith ('../'): f = f[3:]
            d['namelist'].append (f)
        d['defdir'] = 'share'
        self.install_files.append (d)
        return


    def dopat_detcommon_shared_library_settings (self, ll):
        self.detcommon_lib = True
        return


    def dopat_install_runtime (self, ll):
        if len(ll) <= 2: return
        if ll[2] == 'method=-symlink': return
        print ('Bad install_runtime', ll)
        self.err = True
        return


    def dopat_CppUnit (self, ll):
        aa = self.split_args (ll)
        if 'imports' in aa:
            self._expand_macros (aa['imports'])
        # xxx TODO!!!
        return


    def dopat_have_root_headers (self, ll):
        aa = self.split_args (ll)
        headers = aa.get ('root_headers')
        if not headers:
            print ('Unhandled have_root_headers 1', ll)
            self.err = True
            return
        linkdef = 'Root/LinkDef.h'
        headers = headers.split()
        for i in range(len(headers)):
            if headers[i].find ('LinkDef') >= 0:
                linkdef = headers[i]
                del headers[i]
                break
        if not linkdef:
            print ('Unhandled have_root_headers 2', ll)
            self.err = True
            return
        del aa['root_headers']

        lib = aa.get ('headers_lib')
        if not lib:
            print ('Unhandled have_root_headers 3', ll)
            self.err = True
            return
        del aa['headers_lib']

        extra_includes = aa.get ('extra_includes')
        if extra_includes:
            del aa['extra_includes']

        self.root_headers.append ((headers, linkdef, lib))
        return


    def dopat_do_genCLIDDB (self, ll):
        self.libs[-1]['cliddb'] = True
        return



    def split_args (self, ll):
        beg = None
        out = {}
        args = ' '.join (ll[2:])
        lastkey = None
        while True:
            args = args.strip()
            if len(args) == 0: break
            pos = args.find ('=')
            if pos < 0:
                if lastkey:
                    aa = args.split()
                    if len(aa) > 0:
                        val = aa[0]
                        args = ' '.join (aa[1:])
                        out[lastkey] = out[lastkey] + ' ' + val
                        continue
                print ('Bad options1', ll, file=sys.stderr)
                self.err = True
                return None
            key = args[:pos].strip()
            args = args[pos+1:].strip()
            if args[0] == '"' or args[0] == "'":
                pos = args.find (args[0], 1)
                if pos < 0:
                    # No matching close quote.
                    # CMT accepts this without error.  Bleh.
                    val = args[1:]
                    args = ''
                val = args[1:pos]
                args = args[pos+1:]
            else:
                pos = args.find (' ')
                if pos < 0:
                    val = args
                    args = ''
                else:
                    val = args[:pos]
                    args = args[pos+1:]
            out[key] = val.strip()
            lastkey = key
        return out


    def dump (self):
        def _genconf_macros (n):
            v = self._find_macro ('genconfig_configurable' + n)
            if v:
                print ("e['GENCONF_%s'] = '%s'" % (n.upper(), v), file=self.fout)
        _genconf_macros ('ModuleName')
        _genconf_macros ('DefaultName')
        _genconf_macros ('Algorithm')
        _genconf_macros ('AlgTool')
        _genconf_macros ('Auditor')
        _genconf_macros ('Service')

        for p in self.cpppaths:
            print ("e.Append(CPPPATH = '%s')"%p, file=self.fout)
        for p in self.public_cpppaths:
            print ("e.Append(PUBLIC_CPPFLAGS = ' -I%s')"%p, file=self.fout)
        flags = self._find_macro ('use_pp_cppflags')
        if flags:
            print ("e.Append(PUBLIC_CPPFLAGS = ' %s ')"%flags, file=self.fout)
        for p in self.cppflags:
            print ("e.Append(CPPFLAGS = ' %s')"%p, file=self.fout)
        for p in self.cxxflags:
            print ("e.Append(CXXFLAGS = ' %s')"%p, file=self.fout)
        for p in self.libpaths:
            print ("e.Append(LIBPATH = '%s')"%p, file=self.fout)
        for p in self.deps:
            print ("add_deps ('" + p + "')", file=self.fout)
        for p in self.private_deps:
            print ("add_private_deps ('" + p + "')", file=self.fout)
        for p in self.install_jo:
            print ("install_jo (" + _maybequote(p) + ")", file=self.fout)
        for p in self.install_python:
            if p['namelist'] == None:
                p['namelist'] = '*py'
            self.print_call ('install_python', p)
        for p in self.install_data:
            print ("install_data (" + _maybequote(p) + ")", file=self.fout)
        for p in self.install_share:
            print ("install_share (" + _maybequote(p) + ")", file=self.fout)
        for p in self.install_xml:
            self.print_call ('install_xml', p)
        for p in self.install_files:
            self.print_call ('install_files', p)
        extra_srcs = {}
        for (headers, linkdef, lib) in self.root_headers:
            self._refmacro (lib + '_dependencies')
            if lib.endswith ('Lib'): lib = lib[:-3]
            rootcint = '$BUILDDIR/' + lib + '_rootcint.cxx'
            print (lib + '_cint=', end=' ', file=self.fout)
            self.print_call ('build_rootcint',
                             {'headers': headers,
                              'rootcint_name' : rootcint,
                              'linkdef_in': linkdef})
            extra_srcs[lib] = Litstring (lib + '_cint')
        for d in self.libs:
            libname = d.get ('libname', self.packbase)
            extra = self._find_macro(libname + '_dependencies')
            if extra:
                for l in extra.split():
                    if l.endswith('Lib'):
                        l = l[:-3]
                    if True:#l.endswith ('Lib'):
                        d['extra_libs'] = d.get('extra_libs',[]) + [l]
            if self.component and not 'libname' in d:
                d['component'] = True
            if self.extra_libs:
                d['extra_libs'] = d.get('extra_libs', []) + self.extra_libs
            if (self.detcommon_lib or self.linked) and not 'cliddb' in d:
                d['cliddb'] = False
            if d.get('linked'):
                if not 'cliddb' in d: d['cliddb'] = False
                del d['linked']
            ln = libname
            if ln.endswith ('Lib'): ln = ln[:-3]
            if ln in extra_srcs:
                d.setdefault ('extra_srcs', []).append (extra_srcs[ln])

            flags0 = self._find_macro (libname + '_pp_cppflags') or ''
            flags1 = self._find_macro ('lib_' + libname + '_pp_cppflags') or ''
            flags2 = self._find_macro ('use_pp_cppflags') or ''
            flags3 = self._find_macro (libname + '_cppflags') or ''
            flags4 = self._find_macro ('cppflags') or ''
            flags = (flags0 + ' ' + flags1 + ' ' + flags2 + ' ' + flags3 + ' ' + flags4).strip()
            d['parse_flags'] = flags if flags else None
            linkopts1 = self._find_macro (libname + '_linkopts') or ''
            linkopts2 = self._find_macro (libname + 'Lib_shlibflags') or ''
            linkopts3 = self._find_macro (libname + '_shlibflags') or ''
            linkopts4 = self._find_macro (libname + '_use_linkopts') or ''
            linkopts5 = self._find_macro (libname + 'Lib_use_linkopts') or ''
            linkopts6 = self._find_macro (libname + 'linkopts') or ''
            linkopts7 = self._find_macro (libname + 'Liblinkopts') or ''
            linkopts = linkopts1 + linkopts2 + linkopts3 + linkopts4 + linkopts5 + linkopts6 + linkopts7
            if linkopts:
                for o in linkopts.split():
                    if o.startswith ('-l'):
                        if o.endswith('Lib'):
                            o = o[:-3]
                        d.setdefault('extra_libs', []).append (o[2:])
                    elif o.startswith ('-L'):
                        d['parse_flags'] = (d['parse_flags'] or '') + ' ' + o
                    else:
                        print ('Unrecognized linkopt for', libname, ':', o)

            if len(self.libs) == 1 and not d.get('component'):
                d['default'] = True
            self.print_call ('build_lib', d)
        for d in self.dicts:
            m = d['dictname'] + '_dependencies'
            extra = self._find_macro(m)
            if extra:
                d['extra_libs'] = d.get('extra_libs','') + ' ' + extra
            m = d['dictname'] + '_shlibflags'
            extra = self._find_macro(m)
            if extra:
                extra = self._expand_macros (extra)
                for e in extra.split():
                    e = e.replace ('"', '')
                    e = e.strip()
                    if e.startswith ('-l'):
                        d['extra_libs'] = d.get('extra_libs','') + ' ' + e[2:]
                    else:
                        print ('Unhandled shlibflags', m, extra)
                        self.errflag = True

            flags = self._find_macro ('use_pp_cppflags') or ''
            if flags:
                d['parse_flags'] = d.get('parse_flags','') + ' ' + flags
            self.print_call ('build_dict', d)
        if self.poolcnv_args:
            poolcnv_files = []
            for aa in self.poolcnv_args:
                cnv_pfx = ''
                if 'cnv_pfx' in aa:
                    cnv_pfx = aa['cnv_pfx']
                    del aa['cnv_pfx']
                poolcnv_files += self.process_poolcnv (aa)
                if poolcnv_files:
                    linkopts = self._find_macro (self.packbase + 'PoolCnv_shlibflags') or ''
                    extra_libs = []
                    if linkopts:
                        for o in linkopts.split():
                            if o.startswith ('-l'):
                                if o.endswith('Lib'):
                                    o = o[:-3]
                                extra_libs.append (o[2:])
                            #elif o.startswith ('-L'):
                            #    d['parse_flags'] = (d['parse_flags'] or '') + ' ' + o
                            else:
                                print ('Unrecognized linkopt for', libname, ':', o)

                    libname = self.packbase + 'PoolCnv'
                    flags1 = self._find_macro ('lib_' + libname + '_pp_cppflags') or ''
                    flags2 = self._find_macro ('use_pp_cppflags') or ''
                    flags3 = self._find_macro (libname + '_cppflags') or ''
                    flags = (flags1 + ' ' + flags2 + ' ' + flags3).strip()
                    parse_flags = flags if flags else ''

                    self.print_call ('build_poolcnv',
                                     {'names' : poolcnv_files,
                                      'cnv_pfx' : cnv_pfx,
                                      'parse_flags' : parse_flags,
                                      'no_rootmap' : self.no_rootmap,
                                      'extra_libs' : ' '.join (extra_libs)})
        for d in self.sercnvs:
            self.print_call ('build_sercnv', d)
        #if self.sercnv_files:
        #    self.print_call ('build_sercnv',
        #                     {'names' : self.sercnv_files})
        for d in self.bins:
            if self.extra_libs:
                d['libs'] = d.get('libs', []) + self.extra_libs
            deps = self._find_macro (d['name'] + '_dependencies')
            if deps:
                for l in deps.split():
                    if l.endswith ('Lib'): l = l[:-3]
                    d['libs'] = d.get('libs',[]) + [l]
            opts = self._find_macro (d['name'] + '_use_linkopts')
            if not opts:
                opts = self._find_macro (d['name'] + 'linkopts')
            if opts:
                d['parse_flags'] = opts
            self._refmacro (d['name'] + '_dependencies')
            # cf typo in OverlayValidation
            self._refmacro (d['name'] + '_dependenciies')
            self.print_call ('build_bin', d)
        if self.scripts:
            print ("install_scripts ('" + ' '.join (self.scripts) + "')",
                   file=self.fout)
        for d in self.unittests:
            if self.extra_libs:
                d['libs'] = d.get('libs', []) + self.extra_libs
            if self.extra_test_libs:
                d['libs'] = d.get('libs', []) + self.extra_test_libs
            #if self.poolcnv_args:
            #    d['libs'] = d.get('libs', []) + '${PACKBASE}PoolCnv'
            tname = d['tests'][0]
            if tname.endswith('_test'):
                tname = tname[:-5]
            opts1 = self._find_macro (tname + '_testlinkopts') or ''
            opts2 = self._find_macro (tname + '_test_use_linkopts') or ''
            opts = opts1 + ' ' + opts2
            if opts.strip():
                for o in opts.split():
                    if o.startswith ('-L'): continue
                    if o.startswith ('-l'):
                        d['libs'] = d.get('libs', []) + [o[2:]]
                        continue
                    print ('Unknown testlinkopts option:', o)
                    self.errflag = True
            deps1 = self._find_macro (tname + '_test_dependencies') or ''
            deps2 = self._find_macro (tname + '_utest_dependencies') or ''
            deps = deps1 + ' ' + deps2
            deptests = []
            if deps.strip():
                for l in deps.split():
                    if l.endswith('_test'):
                        deptests.append (l[:-5])
                    elif l.endswith('_utest'):
                        deptests.append (l[:-6])
                    else:
                        if l.endswith ('Lib'): l = l[:-3]
                        d['libs'] = d.get('libs',[]) + [l]
            if deptests:
                d['deptests'] = deptests
            self.print_call ('build_tests', d)
        if self.do_ctest:
            self.print_call ('build_ctests', {})
        for d in self.athena_tests:
            self.print_call ('athena_test', d)
        for d in self.python_tests:
            self.print_call ('python_test', d)
        return


    def print_call (self, fn, d):
        lead = '%s (' % fn
        indent = ''
        print (lead, end=' ', file=self.fout)
        for (k, v) in d.items():
            print (('%s%s = %s,' % (indent, k, repr(v))), end=' ', file=self.fout)
            indent = '\n' + ' ' * len (lead)
        print (')', file=self.fout)
        return
        
        


if __name__ == "__main__":
    import sys
    r = Reqreader()
    r.translate (open(sys.argv[1]), sys.stdout)
