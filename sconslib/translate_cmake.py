import os
import string
import sys
import glob
import re
import shlex
from cmakeast import ast


def _maybequote (s):
    if s == None: return ''
    return "'" + s + "'"


def _stripquotes (s):
    if len(s) >= 2 and s[0] == '"' and s[-1] == '"':
        return s[1:-1]
    return s


def _trimlib (s):
    ss = _stripquotes(s)
    if ss.endswith('Lib'):
        return ss[:-3]
    return s


def _all_lowercase (s):
    for c in s:
        if c in string.ascii_uppercase:
            return False
    return True


def _quote_dollar (s):
    if s and s[0] == '$':
        return '$' + s
    return s

def _version_compare (a, b):
    aa = _stripquotes(a).split('.')
    bb = _stripquotes(b).split('.')
    for i in range(max(len(aa), len(bb))):
        av = int(aa[i]) if i < len(aa) else 0
        bv = int(bb[i]) if i < len(bb) else 0
        if av > bv:
            return 1
        elif av < bv:
            return -1
    return 0
                   

# FIXME: Find a better way to do this.
_skip_extra_libs = [ 'DataModel', 'AthenaPoolUtilities', 'EventPrimitives',
                     'GeoPrimitives', 'InDetCondServices', 'MuonCondInterface',
                     'TrkNeutralParameters', 'TrkMeasurementBase',
                     'xAODCnvInterfaces', 'TrigStorageDefinitions',
                     'ByteStreamData_test', 'MagFieldInterfaces',
                     'TrkDetDescrInterfaces', 'ISF_FatrasDetDescrInterfaces',
                     'ISF_FatrasInterfaces',
                     'ISF_FastCaloSimInterfaces',
                     'TrkTruthData', 'TrkToolInterfaces', 'MuonCalibITools',
                     'IDC_OverlayBase', 'ITrackToVertex', 'TrkExInterfaces',
                     'RecoToolInterfaces', 'InDetRecToolInterfaces',
                     'TrkVertexFitterInterfaces', 'EventShapeInterface',
                     'MuonRecToolInterfaces', 'TrkValInterfaces',
                     'TrkFitterInterfaces', 'MuonCombinedToolInterfaces',
                     'ICaloTrkMuIdTools', 'MuidInterfaces', 'iPatInterfaces',
                     'MuGirlInterfaces', 'TrigT1CaloToolInterfaces',
                     'TrigT1CaloCalibToolInterfaces',
                     'FTK_DataProviderInterfaces', 'G4AtlasInterfaces',
                     'InDetCondTools', 'TrigKernel',
                     'nlohmann_json::nlohmann_json', 'nlohmann_json']
def _trim_library_list (libs):
    #l = [_trimlib(l) for l in libs if l not in _skip_extra_libs]
    #if 'GaudiKernel' in l:
    #    l.append ('GaudiPluginService')
    return [_trimlib(l) for l in libs]


class myset (set):
    def add (self, x):
        if type(x) == type([]):
            for v in x:
                set.add (self, v)
        else:
            set.add (self, x)
        return


ok_undefined_variables = ['PATCoreLibDictSource']
            


class CMakeReturn (BaseException):
    pass
class CMakeReader:
    def __init__ (self, package, args={}, addcwd = True):
        self.args = args
        self.fout = None
        self.package = package
        self.packbase = os.path.basename (package) if package else None
        self.err = False
        self.deps = []
        self.private_deps = []
        self.extra_libs = myset()
        self.install_jo = []
        self.install_python = []
        self.install_share = []
        self.install_xml = []
        self.install_files = []
        self.subst_files = []
        self.scripts = []
        self.cpppaths = myset()
        self.public_cpppaths = myset()
        self.public_cppflags = []
        self.cppflags = []
        self.libdefs = []
        self.libs = []
        self.unittests = []
        self.test_locks = {}
        self.script_tests = []
        self.xaod_dicts = []
        self.dicts = []
        self.gen_reflex = []
        self.root_headers = []
        self.bins = []
        self.aliases = []
        self.poolcnvs = []
        self.sercnvs = []
        self.depends = []
        self.copy_files = []
        self.make_dirs = set()
        self.scons_lines = []
        self.extra_tools = []

        self.libpaths = []
        self.variables = {}
        self.macros = {}
        self.functions = {}

        cursource = package or ''
        if addcwd:
            cursource = os.path.join (os.getcwd(), cursource)

        self._define ('CMAKE_PROJECT_NAME', 'Athena')
        self._define ('CMAKE_BUILD_TYPE', 'Debug')
        self._define ('CMAKE_SOURCE_DIR', '.')
        self._define ('CMAKE_CURRENT_SOURCE_DIR', cursource)
        self._define ('CMAKE_CURRENT_LIST_DIR', cursource)
        self._define ('CMAKE_SHARE_OUTPUT_DIRECTORY', '$BUILDNAME/share')
        self._define ('CMAKE_JOBOPT_OUTPUT_DIRECTORY', '$BUILDNAME/joboptions')
        self._define ('CMAKE_RUNTIME_OUTPUT_DIRECTORY', '$BUILDNAME/bin')
        self._define ('CMAKE_DATA_OUTPUT_DIRECTORY', '$BUILDNAME/data')
        self._define ('CMAKE_BINARY_DIR', '$BUILDNAME')
        self._define ('CMAKE_MODULE_PATH', '$BUILDNAME/cmake/modules')
        self._define ('CMAKE_CURRENT_BINARY_DIR', '$BUILDDIR')
        self._define ('CMAKE_INSTALL_SHAREDIR', 'share')
        self._define ('CMAKE_INSTALL_LIBDIR', 'libs')
        self._define ('CMAKE_INSTALL_CMAKEDIR', 'cmake')
        self._define ('CMAKE_INSTALL_DATADIR', 'data')
        self._define ('CMAKE_LIBRARY_OUTPUT_DIRECTORY', 'libs')
        self._define ('CMAKE_FILES_DIRECTORY', '/cmake')
        self._define ('CMAKE_COMMAND', 'true')
        self._define ('CMAKE_SHARED_LIBRARY_PREFIX', 'lib')
        self._define ('CMAKE_SHARED_LIBRARY_SUFFIX', '.so')
        self._define ('ATLAS_PLATFORM', '.')
        self._define ('ATLAS_RELEASE_MODE', 'OFF')
        self._define ('ATLAS_RELEASE_RECOMPILE_DRYRUN', 'OFF')
        self._define ('ATHENAHIVE', 'OFF')
        self._define ('TRIGCONF_STANDALONE', 'OFF')
        self._define ('XAOD_STANDALONE', 'OFF')
        self._define ('XAOD_ANALYSIS', 'OFF')
        self._define ('SIMULATIONBASE', 'OFF')
        self._define ('GENERATIONBASE', 'OFF')
        self._define ('APPLE', 'OFF')
        self._define ('UNIX', 'ON')
        self._define ('CMAKE_Fortran_IMPLICIT_LINK_LIBRARIES', '')
        self._define ('CMAKE_DL_LIBS', 'dl')
        self._define ('RT_LIBRARY', 'rt')
        self._define ('BUILDVP1LIGHT', 'OFF')
        self._define ('AthenaPoolUtilitiesTest_DIR', 'Database/AthenaPOOL/AthenaPoolUtilities/cmake')
        self._define ('xAODUtilities_DIR', 'Event/xAOD/xAODCore/cmake')
        self._define ('ATLAS_FLAKE8', 'true')
        self._define ('HEPMC3_USE', 'ON')
        self._define ('CMAKE_SIZEOF_VOID_P', '8')
        self._define ('CMAKE_AUTOUIC', 'FALSE')
        self._define ('CMAKE_INCLUDE_CURRENT_DIR', 'FALSE')
        self._define ('CMAKE_CXX_COMPILER_VERSION', os.environ['COMPILER_VERSION'])
        self._define ('USE_GPU', 'FALSE')
        self._define ('CMAKE_CUDA_COMPILER', 'FALSE')
        self._define ('ATLAS_GEANT4_USE_LTO', 'FALSE')

        self._define ('ATLAS_OS_ID', 'el9')

        self._define ('ATLAS_BASE_PROJECT_NAMES', 'Athena')

        if os.environ['ARCH'] != '-clang':
            self._define ('CMAKE_COMPILER_IS_GNUCXX', 'YES')
            self._define ('CMAKE_CXX_COMPILER_ID', 'GNU')
        else:
            self._define ('CMAKE_COMPILER_IS_GNUCXX', 'NO')
            self._define ('CMAKE_CXX_COMPILER_ID', 'Clang')

        self._define ('CMAKE_SYSTEM_PROCESSOR', 'x86_64')
        self._define ('CMAKE_HOST_SYSTEM_NAME', 'Linux')
        self._define ('HEPMC_FOUND', 'YES')
        return


    def _error (self, msg):
        print (msg, file=sys.stderr)
        self.err = True
        return False


    class PushVar (object):
        def __init__ (self, reader):
            self.r = reader
            return
        def __enter__ (self):
            self.d = self.r.variables.copy()
            return
        def __exit__ (self, *args):
            self.r.variables = self.d
            return
    def _pushvars (self):
        return CMakeReader.PushVar (self)


    def _expand_target_file (self, ss):
        ss0 = ss
        while True:
            beg = ss.find ('$<TARGET_FILE:')
            if beg < 0: break
            end = ss.find ('>', beg+1)
            if end < 0: break
            target = ss[beg+14:end]
            for b in self.bins:
                if b['name'] == target:
                    root = os.path.basename (os.path.splitext(b['srcs'][0])[0])
                    path = f'$BINDIR/{root}{b['suffix']}'
                    ss = ss[:beg] + path + ss[end+1:]
                    break
        return ss
    def _define (self, name, value):
        self.variables[name] = [value, False]
        return


    def _expand_variables1 (self, ss, beg, end, quote = False):
        ipos = ss.find (beg)
        while ipos >= 0:
            if ipos > 0 and ss[ipos-1] == '\\':
                ss = ss[:ipos-1] + ss[ipos:]
                ipos = ss.find (beg, ipos)
                continue
            epos = ss.find (end, ipos)
            if epos > ipos:
                mname = ss[ipos+len(beg):epos]
                if mname in ['PACKAGE']:
                    ipos = ss.find (beg, ipos+1)
                    continue
                if mname not in self.variables:
                    if not _all_lowercase (mname) and not mname in ok_undefined_variables:
                        print ('Undefined variable: ', mname, file=sys.stderr)
                        self.err = True
                exp = self.variables.get (mname, ['', True])
                exp[1] = True
                val = exp[0]
                epos += len(end)
                #out = self._handle_macro_tags (exp[0])
                if len(val)>1 and val[0] == '"' and val[-1] == '"':
                    # Hack for expanding quoted string inside quoted string.
                    if ((ipos>0 and ss[ipos-1] == '"' and
                         epos<len(ss) and ss[epos] == '"') or
                        (ss[0] == '"' and ss[-1] == '"')):
                        val = val[1:-1]
                    elif epos<len(ss) and ss[epos] in string.ascii_letters:
                        val = val[1:-1]
                    elif ss[ipos-1] == '/':
                        val = val[1:-1]
                elif quote and (len(ss) <= epos or ss[epos] == ' '):
                    val = '"' + val + '"'
                ss = ss[:ipos] + val + ss[epos:]
            #ipos = ss.find (beg, ipos+len(beg))
            ipos = ss.find (beg, ipos)
        return ss
    def _expand_variables (self, ss, quote = False):
        #ss = self._expand_macros1 (ss, '$(', ')')
        ss = self._expand_variables1 (ss, '${', '}', quote = quote)
        ss = self._expand_target_file (ss)
        return ss


    def _getword (self, w):
        if not isinstance (w, ast.Word):
            print ('word expected:', w, file=sys.stderr)
            self.err = True
            return ''
        return w.contents


    def _getstring (self, w):
        if not isinstance (w, ast.Word):
            print ('word expected:', w, file=sys.stderr)
            self.err = True
            return ''
        if w.type == ast.WordType.String and len(w.contents) >= 2 and w.contents[0] == '"' and w.contents[-1] == '"':
            return w.contents[1:-1]
        return w.contents


    def _append_word (self, list, w):
        ww = self._expand_variables (w)
        if ww == w:
            list.append(w)
        else:
            for t in ast.tokenize(ww):
                if t.type == ast.TokenType.Word:
                    list.append (t.content)
                elif t.type == ast.TokenType.QuotedLiteral:
                    list.append (t.content[1:-1])
                elif t.type == ast.TokenType.UnquotedLiteral:
                    list.append (t.content)
                else:
                    assert 0
        return


    def _expand_and_retokenize (self, args_in):
        args = [self._expand_variables(self._getword(a)) for a in args_in]
        l = [t.content for t in ast.tokenize(' '.join(args))]
        return l


    def _expand_virtlibs1 (self, ll):
        incs = []
        out = []
        foundqt5 = False
        for l in ll:
            if l in ['Qt5::Core', 'Qt5::OpenGL', 'Qt5::Gui',
                     'Qt5::PrintSupport', 'Qt5::Network', 'Qt5::Widgets']:
                foundqt5 = True
            elif l == 'GTest::gtest_main':
                incs += self._expand_variables ('${GTEST_INCLUDE_DIRS}}').split()
                i = ll.index ('GTest::gtest_main')
                ll[i] = 'gtest_main'
            elif l == 'Boost::graph':
                i = ll.index ('Boost::graph')
                ll[i] = 'boost_graph'
            elif l == 'CrestApi::CrestApi':
                i = ll.index ('CrestApi::CrestApi')
                ll[i] = 'CrestApiLib'
            else:
                out.append (l)
        if foundqt5:
            ll[:] = out + self._expand_variables ('${QT5_LIBRARIES}').split()
            incs+= self._expand_variables ('${QT5_INCLUDE_DIRS}').split()
        return incs

    def _expand_virtlibs (self, d):
        incs = []
        if 'public_libs' in d:
            incs += self._expand_virtlibs1 (d['public_libs'])
        if 'private_libs' in d:
            incs +=  self._expand_virtlibs1 (d['private_libs'])
        if 'extra_libs' in d:
            incs +=  self._expand_virtlibs1 (d['extra_libs'])
        if 'libs' in d:
            incs +=  self._expand_virtlibs1 (d['libs'])
        return incs


    def _parse (self, a, kwlist):
        args = []
        kw = {}
        this_kw = None
        a_exp = self._expand_and_retokenize (a)
        for w in a_exp:
            w_sq = _stripquotes (w)
            if w_sq in kwlist:
                this_kw = w_sq
                kw.setdefault (this_kw, [])
            elif this_kw == None:
                self._append_word(args, w)
            else:
                self._append_word(kw[this_kw], w)
        return (args, kw)
    


    def dofun_atlas_subdir (self, a):
        if len(a) != 1:
            print ('bad atlas_subdir1:', a, file=sys.stderr)
            sys.err = True
            return
        w = self._getword (a[0])
        if self.packbase == None:
            self.packbase = w
            self.package = w
            self._define ('CMAKE_CURRENT_SOURCE_DIR', w)

        if w != self.packbase:
            print ('bad atlas_subdir2:', a, self.packbase, file=sys.stderr)
            sys.err = True
            return
        return


    def dofun_atlas_depends_on_subdirs (self, a):
        public = True
        (args, kw) = self._parse (a, [])
        for w in args:
            if w == 'PUBLIC':
                public = True
            elif w == 'PRIVATE':
                public = False
            elif w in ['TestPolicy', 'External/AtlasCOOL', 'AtlasPolicy',
                       'AtlasReconstructionRunTime']:
                continue
            elif public:
                self.deps.append (self._expand_variables (w))
            else:
                self.private_deps.append (self._expand_variables (w))
        return


    def _handle_user_package (self, pack):
        fname = None
        pdir = self.variables.get (pack + '_DIR', None)
        if pdir:
            fname = os.path.join (pdir[0], pack + 'Config.cmake')
            if not os.path.exists (fname):
                fname = None

        if not fname: return False
        f = open(fname)
        t = ast.parse (f.read())
        self._runbody (t.statements)
        return True


    def dofun_find_package (self, a):
        if len(a) < 1:
            print ('Empty find_packge', file=sys.stderr)
            self.err = True
        pack = self._getword(a[0])

        if len(a) > 1 and self._getword(a[1]) == 'REQUIRED':
            del a[1]

        if pack == 'Boost':
            self._define ('Boost_INCLUDE_DIRS', '')
            libs = []
            for i in range(1, len(a)):
                if self._getword(a[i]) == 'COMPONENTS':
                    for aa in a[i+1:]:
                        w = self._getword(aa)
                        libs.append ('boost_' + w)
                    break
            self._define ('Boost_LIBRARIES', ' '.join(libs))
            self._define ('Boost_FOUND', 'YES')

        elif pack == 'TBB':
            self._define ('TBB_LIBRARIES', 'tbb')
            self._define ('TBB_INCLUDE_DIRS', '')

        elif pack == 'ROOT':
            self._define ('ROOT_INCLUDE_DIRS', '')
            libs = []
            if len(a) >= 3 and self._getword(a[1]) == 'COMPONENTS':
                for aa in a[2:]:
                    w = self._getword(aa)

                    if w in ['Cintex', 'Cint', 'PyROOT', 'REQUIRED']: continue
                    if w.startswith ('cppyy$'): continue

                    if w == 'VDT':
                        self._define ('VDT_INCLUDE_DIRS', '')
                        self._define ('VDT_LIBRARIES', '')
                        continue
                    
                    libs.append (w)
            self._define ('ROOT_LIBRARIES', ' '.join(libs))
            self._define ('ROOT_FOUND', 'YES')
            self._define ('ROOT_DEFINITIONS', '')

        elif pack == 'Acts':
            self.libpaths.append ('$ACTS_HOME/$BUILDNAME/libs')
            self.cpppaths.add ('$ACTS_HOME/$BUILDNAME/include')
            comps = ['Core']
            for i in range(1, len(a)-1):
                if self._getword(a[i]) == 'COMPONENTS':
                    comps = [self._getword(w) for w in a[i:]]
            incdirs = []
            for w in comps:
                if w.startswith ('Plugin'):
                    w = 'Plugins/' + w[6:]
                incdir = '$ACTS_HOME/%s/include' % w
                self.cpppaths.add (incdir)
                incdirs.append (incdir)

            incdirs.append ('$ACTS_HOME/$BUILDNAME/include')
            self._define ('Acts_INCLUDE_DIR', ' '.join (incdirs))

        elif pack == 'UUID':
            self._define ('UUID_INCLUDE_DIRS', '')
            self._define ('UUID_LIBRARIES', 'uuid')

        elif pack == 'COOL':
            self._define ('COOL_INCLUDE_DIRS', '$COOL_HOME/src/CoolKernel $COOL_HOME/src/CoolApplication $COOL_HOME/src/RelationalCool')
            self._define ('COOL_LIBRARIES', 'lcg_CoolKernel lcg_CoolApplication')
            self.libpaths.append ('$COOL_HOME/$BUILDNAME/libs')

        elif pack == 'CORAL':
            self._define ('CORAL_INCLUDE_DIRS', '$CORAL_HOME/src/CoralBase $CORAL_HOME/src/CoralKernel $CORAL_HOME/src/RelationalAccess')
            self._define ('CORAL_LIBRARIES', 'lcg_CoralKernel lcg_CoralBase lcg_ConnectionService lcg_RelationalAccess uuid')
            self.libpaths.append ('$CORAL_HOME/$BUILDNAME/libs')

        elif pack == 'PythonLibs':
            import sys
            major = sys.version_info.major
            minor = sys.version_info.minor
            tag = "{}.{}".format (major, minor)
            self._define ('PYTHON_INCLUDE_DIRS', '')
            self._define ('PYTHON_LIBRARIES', 'python' + tag)
            self._define ('Python_INCLUDE_DIRS', '')
            self._define ('Python_LIBRARIES', 'python' + tag)

        elif pack == 'Python':
            import sys
            major = sys.version_info.major
            minor = sys.version_info.minor
            tag = "{}.{}".format (major, minor)
            self._define ('PYTHON_INCLUDE_DIRS', '')
            self._define ('PYTHON_LIBRARIES', 'python' + tag)
            self._define ('Python_INCLUDE_DIRS', '')
            self._define ('Python_LIBRARIES', 'python' + tag)

            self._define ('Python_VERSION_MAJOR', str(major))
            self._define ('Python_VERSION_MINOR', str(minor))
            self._define ('Python_EXECUTABLE', '/usr/bin/python3')

        elif pack == 'XercesC':
            self._define ('XERCESC_INCLUDE_DIRS', '')
            self._define ('XERCESC_LIBRARIES', 'xerces-c')

        elif pack == 'CppUnit':
            self._define ('CPPUNIT_INCLUDE_DIRS', '')
            self._define ('CPPUNIT_LIBRARIES', 'cppunit')

        elif pack == 'Xrootd':
            self._define ('XROOTD_INCLUDE_DIRS', '')
            self._define ('XROOTD_LIBRARIES', '')

        elif pack == 'yampl':
            self._define ('YAMPL_INCLUDE_DIRS', '$YAMPL_HOME/include')
            self._define ('YAMPL_LIBRARIES', 'yampl')
            self.libpaths.append ('$YAMPL_HOME/lib')

        elif pack == 'CURL':
            self._define ('CURL_INCLUDE_DIRS', '')
            self._define ('CURL_LIBRARIES', 'curl')

        elif pack == 'RPC':
            self._define ('RPC_FOUND', 'YES')
            self._define ('RPC_INCLUDE_DIRS', '/usr/include/tirpc')
            self._define ('RPC_LIBRARIES', 'tirpc')

        elif pack == 'GTest':
            self._define ('GTEST_INCLUDE_DIRS', '$GOOGLETEST_HOME/include')
            self._define ('GTEST_LIBRARIES', 'gtest')
            self.libpaths.append ('$GOOGLETEST_HOME/lib')

        elif pack == 'GMock':
            self._define ('GMOCK_INCLUDE_DIRS', '$GOOGLETEST_HOME/include')
            self._define ('GMOCK_LIBRARIES', 'gmock')
            #self._define ('GTEST_LIBRARIES', '')
            self.libpaths.append ('$GOOGLETEST_HOME/lib')

        elif pack == 'dSFMT':
            self._define ('DSFMT_INCLUDE_DIRS', '$DSFMT_HOME/include')
            self._define ('DSFMT_LIBRARIES', 'dSFMT')
            self.libpaths.append ('$DSFMT_HOME/$BUILDNAME/libs')
            self.cppflags.append ('-DDSFMT_MEXP=19937')

        elif pack == 'CLHEP':
            self._define ('CLHEP_INCLUDE_DIRS', '$CLHEP_HOME/$BUILDNAME')
            self._define ('CLHEP_DEFINITIONS', '')
            self._define ('CLHEP_LIBRARIES', 'CLHEP-Matrix CLHEP-Vector CLHEP-Random CLHEP-GenericFunctions CLHEP-Geometry CLHEP-Evaluator')
            self.libpaths.append ('$DSFMT_HOME/$BUILDNAME/libs')
            self.cppflags.append ('-DDSFMT_MEXP=19937')
            self._define ('CLHEP_FOUND', 'YES')

        elif pack == 'valgrind':
            self._define ('VALGRIND_INCLUDE_DIRS', '')
            self._define ('VALGRIND_LIBRARIES', '')
            self._define ('VALGRIND_FOUND', 'OFF')

        elif pack == 'fmt':
            self._define ('FMT_INCLUDE_DIRS', '$FMT_HOME/include')
            self._define ('FMT_LIBRARIES', 'fmt')
            self.libpaths.append ('$FMT_HOME/lib64')

        elif pack == 'Eigen':
            self._define ('EIGEN_INCLUDE_DIRS', '$EIGEN_HOME')
            self._define ('EIGEN_INCLUDE_DIR', '$EIGEN_HOME')
            self._define ('EIGEN_LIBRARIES', '')

        elif pack == 'tdaq-common':
            self._define ('TDAQ-COMMON_FOUND', 'YES')
            self._define ('TDAQ-COMMON_LIBRARIES', '')
            self.libpaths.append ('$TDAQ_HOME/$BUILDNAME/libs')
            inc = ['$TDAQ_HOME/eformat', '$TDAQ_HOME/compression', '$TDAQ_HOME/ers']
            if len(a) >= 3 and self._getword(a[1]) == 'COMPONENTS':
                comps = [self._getword(x) for x in a[2:]]
            else:
                comps = ['CTPfragment']
            deps = []
            deps.append ('eformat')
            deps.append ('ers')
            deps.append ('CTPfragment')
            for w in comps:
                if w == 'CTPfragment':
                    inc += ['$TDAQ_HOME/CTPfragment',
                            '$TDAQ_HOME/$BUILDNAME/CTPfragment']
                elif w == 'EventStorage':
                    inc += ['$TDAQ_HOME/EventStorage']
                elif w == 'MuCalDecode':
                    inc += ['$TDAQ_HOME/MuCalDecode']
                elif w == 'hltinterface':
                    inc += ['$TDAQ_HOME/hltinterface']
                elif w == 'circ_proc':
                    inc += ['$TDAQ_HOME/circ']
                elif w == 'dqm_core':
                    inc += ['$TDAQ_HOME/dqm_core']
                elif w == 'eformat_write':
                    deps.append ('eformat_write')
                elif w == 'DataWriter':
                    deps.append ('DataWriter')
                elif w == 'webdaq':
                    deps.append ('webdaq')
                    inc += ['$TDAQ_HOME/webdaq']
                elif w == 'DataReader':
                    inc += ['$TDAQ_HOME/EventStorage']
                    deps.append ('DataReader')
                if w in ['eformat_write', 'RawFileName', 'DataWriter', 'EventStorage']: continue
                #if w == 'circ_proc': w = 'circ'
                deps.append (w)
            self._define ('TDAQ-COMMON_LIBRARIES', ' '.join(deps))
            self._define ('TDAQ-COMMON_INCLUDE_DIRS', ' '.join(inc))

        elif pack == 'tdaq':
            self._define ('TDAQ_FOUND', 'YES')
            self._define ('TDAQ_LIBRARIES', '')
            self.libpaths.append ('$TDAQ_HOME/$BUILDNAME/libs')
            comps = []
            if len(a) >= 3 and self._getword(a[1]) == 'COMPONENTS':
                comps = [self._getword(x) for x in a[2:]]
            inc = []
            deps = []
            for w in comps:
                if w == 'ipc':
                    inc += ['$TDAQ_HOME/ipc']
                    deps.append ('ipc')
                elif w == 'is':
                    inc += ['$TDAQ_HOME/is']
                    deps.append ('is')
                elif w == 'owl':
                    inc += ['$TDAQ_HOME/owl']
                    deps.append ('owl')
                elif w == 'omnithread':
                    pass
                elif w == 'omniORB4':
                    pass
            self._define ('TDAQ_LIBRARIES', ' '.join(deps))
            self._define ('TDAQ_INCLUDE_DIRS', ' '.join(inc))
            self._define ('TDAQ-COMMON_LIBRARIES', ' '.join(deps))
            self._define ('TDAQ-COMMON_INCLUDE_DIRS', ' '.join(inc))
                    
        elif pack == 'dqm-common':
            self._define ('DQM-COMMON_INCLUDE_DIRS', '$TDAQ_HOME/dqm_core')
            self._define ('DQM-COMMON_LIBRARIES', 'dqm_core dqm_core_io')
            
        elif pack == 'FastJet':
            self._define ('FASTJET_INCLUDE_DIRS', '$FASTJET_HOME/include')
            self._define ('FASTJET_LIBRARIES', 'fastjetplugins fastjet fastjettools siscone siscone_spherical EnergyCorrelator Nsubjettiness')
            self.libpaths.append ('$FASTJET_HOME/lib')

        elif pack == 'FastJetContrib':
            self._define ('FASTJETCONTRIB_INCLUDE_DIRS', '$FASTJET_HOME/include')
            self.libpaths.append ('$FASTJET_HOME/lib')
            libs = []
            if len(a) >= 3 and self._getword(a[1]) == 'COMPONENTS':
                for aa in a[2:]:
                    w = self._getword(aa)
                    libs.append (w)
            self._define ('FASTJETCONTRIB_LIBRARIES', ' '.join(libs))

        elif pack == 'ShowerDeconstruction':
            self._define ('SHOWERDECONSTRUCTION_INCLUDE_DIRS', '')
            self._define ('SHOWERDECONSTRUCTION_LIBRARIES', '')
            self._define ('SHOWERDECONSTRUCTION_FOUND', '0')

        elif pack == 'HepMCAnalysis':
            self._define ('HEPMCANALYSIS_INCLUDE_DIRS', '$HEPMCANALYSIS_HOME/include')
            self._define ('HEPMCANALYSIS_LIBRARIES', 'HepMCAnalysis')
            self.libpaths.append ('$HEPMCANALYSIS_HOME/lib')

        elif pack == 'MCTester':
            self._define ('MCTESTER_INCLUDE_DIRS', '$MCTESTER_HOME/include')
            self._define ('MCTESTER_LIBRARIES', 'MCTester')
            self.libpaths.append ('$MCTESTER_HOME/lib')

        elif pack == 'HEPUtils':
            self._define ('HEPUTILS_INCLUDE_DIRS', '$HEPUTILS_HOME/include')
            self._define ('HEPUTILS_INCLUDE_DIR', '$HEPUTILS_HOME/include')
            self._define ('HEPUTILS_LIBRARIES', 'EnergyCorrelator Nsubjettiness VariableR RecursiveTools')
            self.libpaths.append ('$FASTJET_HOME/lib')
            self._define ('HEPUTILS_FOUND', 'YES')
            self._define ('HEPUTILS_LCGVERSION', '1')

        elif pack == 'MCUtils':
            self._define ('MCUTILS_INCLUDE_DIRS', '$MCUTILS_HOME/include')
            self._define ('MCUTILS_INCLUDE_DIR', '$MCUTILS_HOME/include')
            self._define ('MCUTILS_LIBRARIES', '')
            self._define ('MCUTILS_FOUND', 'YES')
            self._define ('MCUTILS_LCGVERSION', '1')

        elif pack == 'HepMC':
            self._define ('HEPMC_INCLUDE_DIRS', '')
            deps = ['HepMC', 'HepPDT', 'HepPID']
            if len(a) >= 3 and self._getword(a[1]) == 'COMPONENTS':
                for aa in a[2:]:
                    w = self._getword(aa)
                    deps.append (w)
            self._define ('HEPMC_LIBRARIES', ' '.join(deps))
            self._define ('HEPMC_FOUND', 'YES')
            self._define ('_HEPMC_HepMC_library', 'HepMC')
            self._define ('_HEPMC_HepMCfio_library', 'HepMCfio')
            self._define ('HEPMC_HepMC_LIBRARY', 'HepMC')
            self._define ('HEPMC_HepMCfio_LIBRARY', 'HepMCfio')
            self._define ('HEPMC3_USE', 'OFF')

        elif pack == 'hepmc3':
            self.libpaths.append ('$HEPMC3_HOME/build$ARCH/libs')
            self._define ('HEPMC_INCLUDE_DIRS', '$HEPMC3_HOME/include $HEPMC3_HOME/search/include')
            self._define ('HEPMC_HepMC3_LIBRARY', 'HepMC3')
            self._define ('HEPMC3_HepMC3_LIBRARY', 'HepMC3')
            self._define ('HEPMC_FOUND', 'YES')
            self._define ('HEPMC3_INCLUDE_DIRS', '$HEPMC3_HOME/include $HEPMC3_HOME/search/include')
            self._define ('HEPMC3_HepMC3search_LIBRARY', 'HepMC3search')
            self._define ('HEPMC_FOUND', 'YES')

        elif pack == 'ZLIB':
            self._define ('ZLIB_INCLUDE_DIRS', '')
            self._define ('ZLIB_LIBRARIES', 'z')
            self._define ('ZLIB_FOUND', 'YES')

        elif pack == 'gdb':
            self._define ('GDB_INCLUDE_DIRS', '')
            self._define ('GDB_LIBRARIES', 'bfd')
            self._define ('GDB_FOUND', 'YES')

        elif pack == 'libunwind':
            self._define ('LIBUNWIND_INCLUDE_DIRS', '')
            self._define ('LIBUNWIND_LIBRARIES', '')
            self._define ('LIBUNWIND_FOUND', 'YES')

        elif pack == 'gperftools':
            deps = []
            if len(a) >= 3 and self._getword(a[1]) == 'COMPONENTS':
                for aa in a[2:]:
                    w = self._getword(aa)
                    if w == 'profiler':
                        deps.append (w)

            self._define ('GPERFTOOLS_INCLUDE_DIRS', '')
            self._define ('GPERFTOOLS_LIBRARIES', ' '.join(deps))
            self._define ('GPERFTOOLS_FOUND', 'YES')

        elif pack == 'GSL':
            self._define ('GSL_INCLUDE_DIRS', '')
            self._define ('GSL_LIBRARIES', 'gsl cblas')

        elif pack == 'HepPDT':
            self._define ('HEPPDT_INCLUDE_DIRS', '')
            self._define ('HEPPDT_LIBRARIES', 'HepPDT HepPID')

        elif pack == 'JavaSDK':
            self._define ('JAVASDK_INCLUDE_DIRS', '')
            self._define ('JAVASDK_LIBRARIES', '')

        elif pack == 'Threads':
            self._define ('THREADS_INCLUDE_DIRS', '')
            self._define ('THREADS_LIBRARIES', '')
            self._define ('CMAKE_THREAD_LIBS_INIT', '')

        elif pack == 'LAPACK':
            self._define ('LAPACK_INCLUDE_DIRS', '')
            self._define ('LAPACK_LIBRARIES', 'lapack')

        elif pack == 'BLAS':
            self._define ('BLAS_INCLUDE_DIRS', '')
            self._define ('BLAS_LIBRARIES', 'blas')

        elif pack == 'AIDA':
            self._define ('AIDA_INCLUDE_DIRS', '')
            self._define ('AIDA_LIBRARIES', '')

        elif pack == 'Davix':
            self._define ('DAVIX_INCLUDE_DIRS', '/usr/include/davix')
            self._define ('DAVIX_LIBRARIES', 'davix')

        elif pack == 'Geant4':
            self._define ('GEANT4_INCLUDE_DIRS', '$GEANT_HOME/include')
            self._define ('GEANT4_LIBRARIES', 'G4physicslists G4materials G4tracking G4run G4processes G4event G4particles G4digits_hits G4track G4geometry G4graphics_reps G4intercoms G4global G4analysis expat')
            self.libpaths.append ('$GEANT_HOME/lib/$G4SYSTEM')
            self.cppflags.append ('-DG4MULTITHREADED')
            self.cppflags.append ('-DG4USE_STD11')
            self.cppflags.append ('-DG4USE_STD17')

        elif pack == 'Tauola':
            self._define ('TAUOLA_INCLUDE_DIRS', '')
            self._define ('TAUOLA_LIBRARIES', '')

        elif pack == 'Photos':
            self._define ('PHOTOS_INCLUDE_DIRS', '')
            self._define ('PHOTOS_LIBRARIES', '')

        elif pack == 'FFTW':
            self._define ('FFTW_INCLUDE_DIRS', '')
            self._define ('FFTW_LIBRARIES', 'fftw3f')

        elif pack == 'Lhapdf':
            self._define ('LHAPDF_INCLUDE_DIRS', '$LHAPDF_HOME/include')
            self._define ('LHAPDF_LIBRARIES', 'LHAPDF')
            self._define ('LHAPDF_FOUND', 'YES')
            self._define ('LHAPDF_LCGVERSION', 'xxx')
            self._define ('LHAPDF_LCGROOT', os.environ['LHAPDF_HOME'])
            self.libpaths.append ('$LHAPDF_HOME/lib')

        elif pack == 'lwtnn':
            self._define ('LWTNN_INCLUDE_DIRS', '$LWTNN_HOME/include')
            self._define ('LWTNN_LIBRARIES', 'lwtnn')
            self.libpaths.append ('$LWTNN_HOME/lib')

        elif pack == 'xml':
            self._define ('XML_INCLUDE_DIRS', '/usr/include/libxml2')
            self._define ('XML_LIBRARIES', 'xml2')

        elif pack == 'dmtcp':
            self._define ('DMTCP_INCLUDE_DIRS', '$DMTCP_HOME/include')

        elif pack == 'Oracle':
            self._define ('ORACLE_INCLUDE_DIRS', '')
            self._define ('ORACLE_LIBRARIES', '')

        elif pack == 'pytools':
            self._define ('PYTOOLS_INCLUDE_DIRS', '')
            self._define ('PYTOOLS_LIBRARIES', '')

        elif pack == 'VDT':
            self._define ('VDT_INCLUDE_DIRS', '')
            self._define ('VDT_LIBRARIES', '')

        elif pack in ['requests', 'graphviz', 'pygraphviz',  'stomppy',
                      'six', 'decorator', 'future', 'nlohmann_json',
                      'pandas', 'matplotlib', 'numpy', 'sqlalchemy',
                      'psutil', 'pprof', 'distro', 'libxkbcommon', 'glib',
                      'libffi', 'VP1AlgsEnvironment', 'zipp',
                      'CxxUtilsEnvironment', 'cx_Oracle', 'pyyaml',
                      'xAODUtilities', 'ipython']:
            pass

        elif pack == 'LibXml2':
            self._define ('LIBXML2_INCLUDE_DIR', '/usr/include/libxml2')
            self._define ('LIBXML2_INCLUDE_DIRS', '/usr/include/libxml2')
            self._define ('LIBXML2_LIBRARIES', 'xml2')

        elif pack == 'GeoModelCore' or pack == 'GeoModel':
            self.libpaths.append ('$GEOMODEL_HOME/$BUILDNAME/libs')
            inc = ['$GEOMODEL_HOME/GeoModelCore/GeoGenericFunctions', '$GEOMODEL_HOME/GeoModelCore/GeoModelKernel']
            deps = ['GeoGenericFunctions', 'GeoModelKernel']
            if len(a) >= 3 and self._getword(a[1]) == 'COMPONENTS':
                for aa in a[2:]:
                    w = self._getword(aa)
                    if w != 'GeoModelKernel':
                        deps.append (w)
                    if w == 'GeoModelDBManager':
                        inc.append ('$GEOMODEL_HOME/GeoModelIO/GeoModelDBManager')
                    if w == 'GeoModelRead':
                        inc.append ('$GEOMODEL_HOME/GeoModelIO/GeoModelRead')
                        inc.append ('$GEOMODEL_HOME/GeoModelCore/GeoModelHelpers')
                    if w == 'GeoModelWrite':
                        inc.append ('$GEOMODEL_HOME/GeoModelIO/GeoModelWrite')
                    if w == 'ExpressionEvaluator':
                        inc.append ('$GEOMODEL_HOME/GeoModelTools/ExpressionEvaluator')
                    if w == 'GeoModelHelpers':
                        inc.append ('$GEOMODEL_HOME/GeoModelCore/GeoModelHelpers')
                    if w == 'GeoModelXml':
                        inc.append ('$GEOMODEL_HOME/GeoModelTools/GeoModelXML/GeoModelXml')
                        inc.append ('$GEOMODEL_HOME/GeoModelCore/GeoModelHelpers')
                        deps.append ('GeoModelHelpers')
            self._define ('GEOMODEL_LIBRARIES', ' '.join (deps))
            self._define ('GEOMODELCORE_LIBRARIES', ' '.join (deps))
            self._define ('GEOMODEL_INCLUDE_DIRS', ' '.join (inc))
            self._define ('GEOMODELCORE_INCLUDE_DIRS', ' '.join (inc))

        elif pack == 'CrestApi':
            self.libpaths.append ('$CRESTAPI_HOME/$BUILDNAME/libs')
            self._define ('CrestApi_INCLUDE_DIRS', '$CRESTAPI_HOME')

        elif pack == 'pyanalysis':
            self._define ('PYANALYSIS_INCLUDE_DIRS', '')
            self._define ('PYANALYSIS_LIBRARIES', '')

        elif pack == 'pygraphics':
            self._define ('PYGRAPHICS_INCLUDE_DIRS', '')
            self._define ('PYGRAPHICS_LIBRARIES', '')

        elif pack == 'EXPAT':
            self._define ('EXPAT_INCLUDE_DIRS', '')
            self._define ('EXPAT_LIBRARIES', 'expat')

        elif pack == 'SQLite3':
            self._define ('SQLITE3_INCLUDE_DIRS', '')
            self._define ('SQLITE3_LIBRARIES', 'sqlite3')

        elif pack == 'onnxruntime':
            self._define ('ONNXRUNTIME_INCLUDE_DIRS', '$ONNXRUNTIME_HOME/include/onnxruntime $ONNXRUNTIME_HOME/include/onnxruntime/core/session')
            self._define ('ONNXRUNTIME_LIBRARIES', 'onnxruntime')
            self._define ('onnxruntime_LIBRARIES', '')
            self._define ('onnxruntime_LIBRARY', '')
            self._define ('onnxruntime_INCLUDE_DIR', '')
            self.libpaths.append ('$ONNXRUNTIME_HOME/lib64')

        elif pack == 'Rivet':
            self._define ('RIVET_INCLUDE_DIRS', '$RIVET_HOME/include')
            self._define ('RIVET_LIBRARIES', 'Rivet')
            self.libpaths.append ('$RIVET_HOME/lib')

        elif pack == 'YODA':
            self._define ('YODA_INCLUDE_DIRS', '$YODA_HOME/include')
            self._define ('YODA_LIBRARIES', 'YODA')
            self.libpaths.append ('$YODA_HOME/lib')

        elif pack == 'Coin3D':
            self._define ('COIN3D_INCLUDE_DIRS', '/usr/include/Coin4')
            self._define ('COIN3D_LIBRARIES', 'Coin')
            self._define ('OPENGL_LIBRARIES', '')

        elif pack == 'Qt5':
            comps = []
            if len(a) >= 3 and self._getword(a[1]) == 'COMPONENTS':
                comps = ['Qt' + self._getword(aa) for aa in a[2:]]
                comps5 = ['Qt5' + self._getword(aa) for aa in a[2:]]
            qtincdir = '/usr/include/qt5'
            inc = [qtincdir] + [os.path.join (qtincdir, c) for c in comps]
            self._define ('QT5_INCLUDE_DIRS', ' '.join (inc))
            qtlibs = ['Qt5Core', 'Qt5OpenGL', 'Qt5Gui', 'Qt5Network', 'Qt5PrintSupport']
            clibs = [l for l in comps5 if l in qtlibs]
            self._define ('QT5_LIBRARIES', ' '.join (clibs))
            self.extra_tools += ['qt5']

        elif pack == 'SoQt':
            self._define ('SOQT_INCLUDE_DIRS', '')
            self._define ('SOQT_LIBRARIES', 'SoQt')

        elif pack == 'HDF5':
            self._define ('HDF5_CXX_INCLUDE_DIRS', '')
            self._define ('HDF5_CXX_DEFINITIONS', '')
            self._define ('HDF5_INCLUDE_DIRS', '')
            self._define ('HDF5_LIBRARIES', 'hdf5_cpp hdf5')
            self._define ('HDF5_HL_LIBRARIES', 'hdf5_hl hdf5_hl_cpp')
            self._define ('HDF5_CXX_LIBRARIES', 'hdf5_cpp')

        elif pack == 'HighFive':
            self._define ('HIGHFIVE_INCLUDE_DIRS', '')
            self._define ('HIGHFIVE_FOUND', 'YES')

        elif pack == 'Pythia8':
            self._define ('PYTHIA8_FOUND', 'YES')
            self._define ('PYTHIA8_LCGROOT', os.environ['PYTHIA8_HOME'])
            self._define ('PYTHIA8_INCLUDE_DIRS', '$PYTHIA8_HOME/include')
            self._define ('PYTHIA8_INCLUDE_DIR', '$PYTHIA8_HOME/include')
            self._define ('PYTHIA8_LIBRARIES', 'pythia8')
            self._define ('PYTHIA8_LCGVERSION', 'xxx')
            self.libpaths.append ('$PYTHIA8_HOME/lib')

        elif self._handle_user_package (pack):
            pass
        
        else:
            import sys
            print ('unknown package:', pack, file=sys.stderr)
            self.err = True

        return


    def dofun_include (self, a):
        (args, kw) = self._parse (a, [])
        for arg in args:
            if arg in ['CMakeParseArguments']:
                pass
            else:
                print ('unhandled include:', arg)
                self.err = True
        return


    def dofun_atlas_add_library (self, a):
        (args, kw) = self._parse (a, ['NO_PUBLIC_HEADERS',
                                      'PUBLIC_HEADERS',
                                      'INTERFACE',
                                      'SHARED',
                                      'STATIC',
                                      'OBJECT',
                                      'MODULE',
                                      'INCLUDE_DIRS',
                                      'PRIVATE_INCLUDE_DIRS',
                                      'LINK_LIBRARIES',
                                      'PRIVATE_LINK_LIBRARIES',
                                      'PRIVATE_DEFINITIONS',
                                      'DEFINITIONS'])

        pubheaders = kw.get('PUBLIC_HEADERS', [])
        if pubheaders and pubheaders != [self.packbase]:
            print ('unhandled PUBLIC_HEADERS', file=sys.stderr)
            self.err = True
            return
        if kw.get('MODULE', None):
            print ('add_library used with MODULE', file=sys.stderr)
            self.err = True
            return

        interface_p = (kw.get('INTERFACE', None) != None or len(args) < 2)
        pub_libs = []
        for l in kw.get('LINK_LIBRARIES', []):
            pub_libs += _stripquotes(l).split()
        priv_libs = kw.get('PRIVATE_LINK_LIBRARIES', [])
        include_dirs = [p for p in kw.get('INCLUDE_DIRS', []) if p and p != '""']
        d = {'libname' : args[0],
             'interface' : interface_p,
             'pubheaders' : pubheaders,
             'public_libs' : pub_libs,
             'private_libs' : priv_libs,
             'public_includes' : include_dirs,
             'private_includes' : kw.get('PRIVATE_INCLUDE_DIRS', []),
             'defines' : kw.get('DEFINITIONS', []) }
        d['public_includes'] += self._expand_virtlibs (d)
        self.libdefs.append (d)

        self.public_cpppaths.add (include_dirs)

        if interface_p:
            return

        d = {'libname' : args[0],
             'srcs' : ' '.join(args[1:]),
             'no_components' : True,
             'extra_libs' : [],
             'cliddb' : False,
             }

        # FIXME: For now, treat OBJECT the same as STATIC.
        #  ... on second thought, just treat it as SHARED for now.
        #if kw.get('STATIC', None) is not None or kw.get('OBJECT', None) is not None:
        #    d['static'] = True

        d['extra_libs'] += pub_libs
        d['extra_libs'] += priv_libs
        d['extra_libs'] = _trim_library_list (d['extra_libs'])
        pf = kw.get('DEFINITIONS', [])
        pf = [x if _stripquotes(x).startswith('-D') else '-D'+x for x in pf]
        if pf:
            d['parse_flags'] = d.get('parse_flags','') + ' ' + ' '.join(pf)
            self.public_cppflags += pf
        pf = kw.get('PRIVATE_DEFINITIONS', [])
        pf = [x if _stripquotes(x).startswith('-D') else '-D'+x for x in pf]
        if pf:
            d['parse_flags'] = d.get('parse_flags','') + ' ' + ' '.join(pf)
            #self.cppflags += pf
        self.cpppaths.add (kw.get('PRIVATE_INCLUDE_DIRS', []))
        self.libs.append (d)
        return d

    
    def dofun_atlas_add_component (self, a):
        (args, kw) = self._parse (a, ['NOCLIDDB',
                                      'INCLUDE_DIRS',
                                      'PRIVATE_INCLUDE_DIRS',
                                      'LINK_LIBRARIES',
                                      'PRIVATE_LINK_LIBRARIES',
                                      'PUBLIC_HEADERS',
                                      'NO_PUBLIC_HEADERS',
                                      'DEFINITIONS'])

        if len(args) < 2:
            print ('bad add_component args', file=sys.stderr)
            self.err = True
            return

        d = {'libname' : args[0] + '_components',
             'srcs' : ' '.join(args[1:]),
             'component' : True,
             'extra_libs' : [],
             'cliddb' : True,
             }

        pubheaders = kw.get('PUBLIC_HEADERS', None)
        if pubheaders and pubheaders != [self.packbase]:
            print ('unhandled PUBLIC_HEADERS', file=sys.stderr)
            self.err = True
            return

        if kw.get('NOCLIDDB', None) != None:
            d['cliddb'] = False

        d['extra_libs'] += _trim_library_list (kw.get('LINK_LIBRARIES', []))
        d['extra_libs'] += _trim_library_list (kw.get('PRIVATE_LINK_LIBRARIES', []))
        self.cpppaths.add (kw.get('PRIVATE_INCLUDE_DIRS', []))
        self.cpppaths.add (kw.get('INCLUDE_DIRS', []))
        pf = kw.get('DEFINITIONS', [])
        if pf:
            pf_out = []
            for f in pf:
                if not f: continue
                if not f.startswith ('-D') and not f.startswith ('"-D'):
                    f = '-D' + f
                pf_out.append (f)
            d['parse_flags'] = ' '.join(pf_out)
        self.cpppaths.add (self._expand_virtlibs (d))
        self.libs.append (d)
        return


    def dofun_atlas_generate_componentslist (self, a):
        (args, kw) = self._parse (a, [])
        if len(args) != 1:
            print ('bad generate_componentslist', file=sys.stderr)
            self.err = True
            return

        lib = args[0]
        for d in self.libs:
            if d.get('libname', None) == lib:
                d['rootmap' ] = True
                return

        print ('bad generate_componentslist: cannot find library', lib, self.libs, file=sys.stderr)
        self.err = True
        return


    def dofun_atlas_generate_cliddb (self, a):
        (args, kw) = self._parse (a, [])
        if len(args) != 1:
            print ('bad generate_cliddb', file=sys.stderr)
            self.err = True
            return

        lib = args[0]
        for d in self.libs:
            if d.get('libname', None) == lib:
                d['cliddb' ] = True
                return

        print ('bad generate_cliddb: cannot find library', lib, file=sys.stderr)
        self.err = True
        return


    def dofun_atlas_add_tpcnv_library (self, a):
        d = self.dofun_atlas_add_library (a)
        d['rootmap'] = True
        return


    def dofun_atlas_add_xaod_smart_pointer_dicts (self, a):
        (args, kw) = self._parse (a, ['INPUT',
                                      'OUTPUT',
                                      'CONTAINERS',
                                      'OBJECTS'])
        if len(args) != 0 or len(kw['INPUT']) != 1 or len(kw['OUTPUT']) != 1:
            print ('bad atlas_add_xaod_smart_pointer_dicts args', a, file=sys.stderr)
            self.err = True
            return

        selfile_in = kw['INPUT'][0]
        outvar = kw['OUTPUT'][0]

        selbase = os.path.basename (selfile_in)
        selfile_out = '$DICTBUILDDIR/' + selbase

        d = {}
        d['selfile_in'] = '$PACKAGE/' + selfile_in
        d['selfile_out'] = selfile_out
        d['objects'] = [_stripquotes(x) for x in kw.get ('OBJECTS', [])]
        d['containers'] = [_stripquotes(x) for x in kw.get ('CONTAINERS', [])]

        self._define (outvar, selfile_out)

        self.xaod_dicts.append (d)
        return

    
    def dofun_atlas_add_dictionary (self, a):
        (args, kw) = self._parse (a, ['INCLUDE_DIRS',
                                      'LINK_LIBRARIES',
                                      'EXTRA_FILES',
                                      'NAVIGABLES',
                                      'DATA_LINKS',
                                      'ELEMENT_LINKS',
                                      'ELEMENT_LINK_VECTORS',
                                      'NO_ROOTMAP_MERGE',
                                      'OPTIONS'])

        if len(args) != 3:
            print ('bad atlas_add_dictionary args', a, file=sys.stderr)
            self.err = True
            return

        d = {}
        d['dictname'] = args[0]
        # if not d['dictname'].endswith ('Dict'):
        #     d['dictname'] += 'Dict'

        header = args[1]
        if header != '%s/%sDict.h' % (self.packbase, self.packbase):
            if not header.endswith ('.h'):
                return self._error ('Unhandled atlas_add_dictionary 3a ' + a)
            d['dictfile']  = '$PACKAGE/' + header

        selfile = _stripquotes (args[2])
        if selfile != '%s/selection.xml' % self.packbase:
            if not selfile.endswith ('.xml'):
                return self._error ('Unhandled atlas_add_dictionary 3b ' + str(a))
            if selfile[0] == '$':
                d['selfile'] = selfile
            else:
                d['selfile'] = '$PACKAGE/' + selfile

        d['dataLinks'] = kw.get('DATA_LINKS', [])
        d['elementLinks'] = kw.get('ELEMENT_LINKS', [])
        d['elementLinkVectors'] = kw.get('ELEMENT_LINK_VECTORS', [])
        d['navigables'] = kw.get('NAVIGABLES', [])
        d['extra_srcs'] = kw.get('EXTRA_FILES', [])
        d['parse_flags'] = kw.get('OPTIONS', '')

        d['extra_libs'] = _trim_library_list (kw.get('LINK_LIBRARIES', []))
        self.cpppaths.add (kw.get('INCLUDE_DIRS', []))

        if kw.get('NO_ROOTMAP_MERGE', False) != False:
            d['rootmap'] = False

        # if aa.get('extraselection'):
        #     f = self._expand_variables (aa['extraselection'])
        #     d['extraselection'] = [f]
        #     del aa['extraselection']

        self.dicts.append (d)

        return

    
    def dofun_atlas_add_executable (self, a):
        (args, kw) = self._parse (a, ['INCLUDE_DIRS',
                                      'LINK_LIBRARIES'])

        if len(args) < 2:
            print ('not enough args to atlas_add_executable', a, file=sys.stderr)
            self.err = True
            return

        if args[1] == 'SOURCES': del args[1]

        d = {'name' : args[0],
             'srcs' : args[1:],
             'suffix' : '',
             'libs' : []}

        self.cpppaths.add (kw.get('INCLUDE_DIRS', []))
        libs = _trim_library_list (kw.get('LINK_LIBRARIES', []))
        self._expand_virtlibs1 (libs)
        d['libs'] += libs

        self.bins.append (d)
        return

    
    def dofun_atlas_install_joboptions (self, a):
        (args, kw) = self._parse (a, ['POST_BUILD_CMD',
                                      'EXCLUDE',])
        self.install_jo.append ({'namelist' : args,
                                 'exclude' : kw.get ('EXCLUDE', [])})
        return

    
    def dofun_atlas_install_python_modules (self, a):
        for aa in a:
            w = self._getword (aa)
            if w == 'POST_BUILD_CMD':
                break
            self.install_python.append ({'namelist' : self._getword (aa)})
        return

    
    def dofun_atlas_install_scripts (self, a):
        for aa in a:
            w = self._getword (aa)
            if w == 'POST_BUILD_CMD':
                break
            self.scripts.append (self._getword (aa))
        return

    
    def dofun_atlas_install_runtime (self, a):
        for aa in a:
            w = self._getword (aa)
            if w == 'POST_BUILD_CMD':
                break
            self.install_share.append (w)
        return

    
    def dofun_atlas_install_xmls (self, a):
        for aa in a:
            self.install_xml.append (self._getword (aa))
        return


    def dofun_atlas_install_generic (self, a):
        (args, kw) = self._parse (a, ['DESTINATION',
                                      'EXECUTABLE',
                                      'TYPENAME',
                                      'BUILD_DESTINATION',
                                      'PKGNAME_SUBDIR'])
        destdir = kw.get ('DESTINATION', [])
        if len(destdir) != 1:
            return self._error ('install_generic: bad destination: ' + str(destdir))
        destdir = destdir[0]
        if 'PKGNAME_SUBDIR' in kw:
            destdir = os.path.join (destdir, self.packbase)
        # Ignoring EXECUTABLE for now.
        d = { 'destdir' : os.path.join ('$BUILDNAME', destdir),
              'defdir' : 'xxxxx',
              'namelist' : args }
        self.install_files.append (d)
        return


    def dofun_atlas_install_data (self, a):
        (args, kw) = self._parse (a, [])
        d = {}
        d['destdir'] = "$SHAREDIR/$PACKBASE"
        d['namelist'] = args
        d['defdir'] = 'share'
        self.install_files.append (d)
        return


    def atlas_add_script_test (self, args, kw, d, locked, deps, workdir):
        if (len(kw.get('SCRIPT')) < 1 or
            kw.get('SOURCES', None) != None or
            kw.get('INCLUDE_DIRS', None) != None or
            kw.get('LINK_LIBRARIES', None) != None):
            return self._error ('bad add_test script args: %s' % kw)
        d['test_cmd'] = kw.get('SCRIPT')
        if len(d['test_cmd']) == 1:
            s = d['test_cmd'][0]
            if s[0] == '"' and s[-1] == '"':
                d['test_cmd'] = s[1:-1].split()
        d['test_cmd'] = d['test_cmd'][:1] +  [_quote_dollar (x) for x in d['test_cmd'][1:]]
        d['name'] = _stripquotes (args[0])
        #if deps:
        #    d['has_deps'] = True
        self.script_tests.append (d)

        self._handle_test_deps (d['name'], locked, deps, workdir)
        return


    def _handle_test_deps (self, name, locked, deps, workdir):
        if locked or deps:
            if not workdir:
                workdir = '$TESTBUILDDIR'
            targ_diff = f'{workdir}/{name}.diff'

            deplist = []
            for dd in deps:
                if dd.endswith ('_ctest'):
                    dd = self._striptest (dd)
                    for tt in self.script_tests:
                        if tt['name'] == dd:
                            if 'workdir' in tt:
                                deplist.append (f'{tt["workdir"]}/{dd}.diff')
                            else:
                                deplist.append ('$TESTBUILDDIR/%s.diff' % dd)
                            break
                    else:
                        deplist.append ('$TESTBUILDDIR/%s.diff' % dd)
                else:
                    deplist.append (dd)

            if locked:
                dd = self.test_locks.get (locked, None)
                if dd:
                    deplist.append (dd)
                self.test_locks[locked] = targ_diff
            if deplist:
                self.depends.append ((targ_diff, deplist))
        return


    def dofun_atlas_add_test (self, a):
        (args, kw) = self._parse (a, ['SOURCES',
                                      'SCRIPT',
                                      'INCLUDE_DIRS',
                                      'LINK_LIBRARIES',
                                      'EXTRA_PATTERNS',
                                      'LOG_SELECT_PATTERN',
                                      'LOG_IGNORE_PATTERN',
                                      'PROPERTIES',
                                      'PRIVATE_WORKING_DIRECTORY',
                                      'PRE_EXEC_SCRIPT',
                                      'POST_EXEC_SCRIPT',
                                      'DEPENDS',
                                      'ENVIRONMENT',
                                      'EXTRADB'])

        name = _stripquotes (args[0])
        workdir = None
        d = {}
        if kw.get('EXTRA_PATTERNS', None):
            ee = [_stripquotes(e) for e in kw.get('EXTRA_PATTERNS')]
            d['extrapatterns'] = ' '.join (ee)
        if kw.get('LOG_SELECT_PATTERN', None):
            ee = [_stripquotes(e) for e in kw.get('LOG_SELECT_PATTERN')]
            d['select_pattern'] = ' '.join (ee)
        if kw.get('LOG_IGNORE_PATTERN', None):
            ee = [_stripquotes(e) for e in kw.get('LOG_IGNORE_PATTERN')]
            d['ignore_pattern'] = ' '.join (ee)
        if 'PRIVATE_WORKING_DIRECTORY' in kw:
            workdir = '$BUILDDIR/tests/' + name + '_test'
        if 'EXTRADB' in kw:
            d['extradb'] = kw.get('EXTRADB')[0]
        if kw.get('ENVIRONMENT', None):
            env = [_stripquotes(x) for x in kw.get('ENVIRONMENT')]
            env = [x.replace('$ENV{JOBOPTSEARCHPATH}',
                             '$JOBOPTSEARCHPATH') for x in env]
            env = [x.replace('$ENV{PATH}',
                             '$$PATH') for x in env]
            env = [x.replace('$ENV{DATAPATH}',
                             '$$DATAPATH') for x in env]
            env = [x.replace('$ENV{FRONTIER_SERVER}',
                             '"' + os.environ['FRONTIER_SERVER'] + '"') for x in env]
            d['environment'] = env
        postsh = kw.get('POST_EXEC_SCRIPT', [])
        if postsh:
            postsh = ' '.join (postsh)
            l = _stripquotes (postsh).split()
            postsh = postsh.replace ('\\\n', ' ')
            d['postsh' ] = postsh

        pre_exec_script = kw.get('PRE_EXEC_SCRIPT', None)
        if pre_exec_script:
            ee = [_stripquotes(e) for e in pre_exec_script]
            d['pre_exec_script'] = ' '.join (ee)

        locked = None
        deps = self.args.get ('TestDepends', [])
        indeps = False
        props = kw.get('PROPERTIES', [])
        while len (props) > 0:
            if props[0] == 'TIMEOUT' and len(props) >= 2:
                props = props[2:]
                indeps = False
            elif props[0] == 'RESOURCE_LOCK' and len(props) >= 2:
                locked = props[1]
                props = props[2:]
                indeps = False
            elif props[0] == 'PROCESSORS' and len(props) >= 2:
                props = props[2:]
                indeps = False
            elif props[0] == 'WORKING_DIRECTORY' and len(props) >= 2:
                workdir = props[1]
                props = props[2:]
                indeps = False
            elif props[0] == 'PRIVATE_WORKING_DIRECTORY':
                workdir = '$BUILDDIR/tests/' + name + '_test'
                props = props[1:]
                indeps = False
            elif props[0] == 'DEPENDS':
                indeps = True
                props = props[1:]
            elif props[0] == 'DISABLED' and len(props) >= 2 and props[1] == 'True':
                return
            elif props[0] == 'WILL_FAIL' and len(props) >= 2 and self._parse_asbool (props[1]):
                return
            elif indeps:
                deps.append (_stripquotes (props[0]))
                props = props[1:]
            else:
                return self._error ('bad test properties: %s' % kw)

        for dep in kw.get('DEPENDS', []):
            deps.append (f'{self.packbase}_{dep}_ctest')

        if workdir:
            d['workdir'] = workdir
        if kw.get('SCRIPT', None) != None:
            return self.atlas_add_script_test (args, kw, d, locked, deps, workdir)

        extra_libs = []
        sources = kw.get('SOURCES', [])
        if 'src/*.cxx' in sources:
            extra_libs = [self.packbase]
        sources = [x for x in sources if x != 'src/*.cxx']

        if len(args) < 1 or len(sources) < 1:
            print ('bad add_test args', args, file=sys.stderr)
            self.err = True
            return

        if not sources[0].startswith('test/'):
            for i in range(1, len(sources)):
                if sources[i].startswith('test/'):
                    sources = sources[i:i+1] + sources[0:i] + sources[i+1:]
                    break
        d['tests'] = [name]
        d['sources'] = [sources[0]]
        d['libs'] = _trim_library_list (kw.get('LINK_LIBRARIES', [])) + extra_libs

        if len(sources) > 1:
            d['extra_sources'] = sources[1:]


        self._handle_test_deps (d['tests'][0], locked, deps, workdir)

        for idir in kw.get('INCLUDE_DIRS', []):
            if idir.find ('/') < 0:
                idir = '$PACKAGE/' + idir
            self.cpppaths.add (idir)

        self.cpppaths.add (self._expand_virtlibs (d))
        self.unittests.append (d)

        return


    def _striptest (self, n):
        if n.startswith (self.packbase + '_'):
            n = n[len(self.packbase)+1:]
        if n.endswith ('_ctest'):
            n = n[:-6]
        return n
        
    def dofun_set_tests_properties (self, a):
        (args, kw) = self._parse (a, ['PROPERTIES'])
        if len(args) != 1:
            return self._error ('set_test_properties1')
        targ = self._striptest (args[0])
        targ_diff = '$TESTBUILDDIR/%s.diff' % targ
        props = kw['PROPERTIES']
        if len(props) < 1:
            return self._error ('set_test_properties2 %s' % kw)

        if props[0] == 'TIMEOUT':
            return

        if props[0] == 'DISABLED':
            return

        if props[0] == 'LABELS':
            return

        if props[0] != 'DEPENDS':
            return self._error ('set_test_properties3 %s' % kw)
        deps = []
        for d in props[1:]:
            d = _stripquotes (d)
            for ddd in d.split(';'):
                for dd in ddd.split():
                    if not dd.endswith ('_ctest'):
                        return self._error ('set_test_properties4 %s' % dd)
                    dd = self._striptest (dd)
                    deps.append ('$TESTBUILDDIR/%s.diff' % dd)

        self.depends.append ((targ_diff, deps))
        return


    def dofun_set_target_properties (self, a):
        (args, kw) = self._parse (a, ['PROPERTIES'])
        if len(kw['PROPERTIES']) < 1:
            return self._error ('set_target_properties2 %s' % kw)
        if kw['PROPERTIES'][0] == 'INSTALL_RPATH' and args[0] == 'vp1light':
            return
        if kw['PROPERTIES'][0] == 'INTERPROCEDURAL_OPTIMIZATION':
            if kw['PROPERTIES'][1] == 'FALSE':
                return
        if len(args) != 1:
            return self._error ('set_target_properties1 %s %s' % (self.package, kw))
        if kw['PROPERTIES'][0] == 'ENABLE_EXPORTS':
            return
        if kw['PROPERTIES'][0] == 'COMPILE_FLAGS':
            for d in self.libs:
                if d['libname'] == args[0]:
                    flags = ' '.join ([_stripquotes(x) for x in kw.get('PROPERTIES')[1:]])
                    d['parse_flags'] = d.get('parse_flags', '') + ' ' + flags
            return
        if kw['PROPERTIES'][0] == 'LINK_FLAGS':
            for d in self.libs:
                if d['libname'] == args[0]:
                    flags = ' '.join ([_stripquotes(x) for x in kw.get('PROPERTIES')[1:]])
                    d['link_flags'] = d.get('link_flags', '') + ' ' + flags
            return
        return self._error ('set_target_properties3 %s %s' % (kw, args))


    def dofun_target_include_directories (self, a):
        (args, kw) = self._parse (a, ['PUBLIC'])
        if len(args) != 1:
            return self._error ('target_include_directories1')
        for d in self.libdefs:
            if d['libname'] == args[0]:
                include_dirs = [p for p in kw.get('PUBLIC', []) if p and p != '""']
                d['public_includes'] += include_dirs
                return
        return self._error ('target_include_directories2')
        


    def dofun_atlas_add_root_dictionary (self, a):
        (args, kw) = self._parse (a, ['ROOT_HEADERS',
                                      'EXTERNAL_PACKAGES',
                                      'INCLUDE_PATHS'])

        if len(args) < 2:
            return self._error ('bad atlas_add_root_dictionary1 %s' % a)

        mainlib = args[0]
        dictvar = args[1]

        headers = kw.get('ROOT_HEADERS', [])
        self.cpppaths.add (kw.get('INCLUDE_DIRS', []))
        extpacks = kw.get ('EXTERNAL_PACKAGES', [])
        if extpacks:
            # Not trying to implement this for now, but warn if it's anything
            # other than root.
            for e in extpacks:
                if e not in ['ROOT', 'HepPDT', 'CLHEP', 'HepMC', 'Eigen',
                             'Boost', 'dqm-common', 'tdaq-common', 'COOL',
                             'CORAL', 'LibXml2', 'HepPDT','HepMC', 'TBB',
                             'XercesC', 'Geant4', 'nlohmann_json']:
                    self._error ('bad atlas_add_root_dictionary2 %s' % a)

        linkdef = 'Root/LinkDef.h'
        for i in range(len(headers)):
            if headers[i].find ('*') >= 0:
                for p in glob.glob (headers[i], root_dir = self.package):
                    if p.find ('LinkDef') >= 0:
                        linkdef = p
                        break
            if headers[i].find ('LinkDef') >= 0:
                linkdef = headers[i]
                del headers[i]
                break

        libroot = mainlib
        if libroot.endswith ('Lib'): libroot = libroot[:-3]
        rootcint = '$BUILDDIR/' + libroot + '_rootcint.cxx'
        self._define (dictvar, rootcint)
        
        self.root_headers.append ((headers, linkdef, mainlib, rootcint))
        return


    def dofun_atlas_generate_reflex_dictionary (self, a):
        (args, kw) = self._parse (a, ['HEADER',
                                      'SELECTION',
                                      'LINK_LIBARIES',
                                      'INCLUDE_DIRS',
                                      'NO_ROOTMAP_MERGE'])

        if len(args) != 2:
            print ('bad atlas_generate_reflex_dictionary args', a, file=sys.stderr)
            self.err = True
            return

        d = {'dont_add_dict' : True}

        dictvar = args[0]
        d['dictname'] = args[1]

        cursource = self._expand_variables ('${CMAKE_CURRENT_SOURCE_DIR}')

        header = kw.get ('HEADER', [])
        if header:
            header = header[0]
            if header.startswith (cursource):
                header = header[len(cursource)+1:]
            if header != '%s/%sDict.h' % (self.packbase, self.packbase):
                if not header.endswith ('.h'):
                    return self._error ('Unhandled atlas_generate_reflex_dictionary 3a ' + a)
                if not header.startswith ('/'):
                    header = '$PACKAGE/' + header
                d['dictfile']  = header

        selfile = kw.get ('SELECTION', [])
        if selfile:
            selfile = selfile[0]
            if selfile.startswith (cursource):
                selfile = selfile[len(cursource)+1:]
            if selfile != '%s/selection.xml' % self.packbase:
                if not selfile.endswith ('.xml'):
                    return self._error ('Unhandled atlas_generate_reflex_dictionary 3b ' + a)
                if not selfile.startswith ('/'):
                    selfile = '$PACKAGE/' + selfile
                d['selfile'] = '$PACKAGE/' + selfile

        self.cpppaths.add (kw.get('INCLUDE_DIRS', []))
        if kw.get('NO_ROOTMAP_MERGE', False) != False:
            d['rootmap'] = False

        self.gen_reflex.append (d)

        dictroot = d['dictname']
        reflex_file = '\\\\${DICTBUILDDIR}' + dictroot + '/' + dictroot + '_gen.cxx'
        self._define (dictvar, reflex_file)
        return


    def _poolcnvns (self, f, ns, mult):
        fbase = os.path.splitext (os.path.split (f)[-1])[0]
        ismult = (fbase in mult)
        for n in ns:
            if n.endswith ('::' + fbase):
                ismult = ismult or (n in mult)
                if ismult: n = '*' + n
                return (f, n)
        if ismult:
            return (f, '*' + fbase)
        return f


    def dofun_atlas_add_poolcnv_library (self, a):
        (args, kw) = self._parse (a, ['FILES',
                                      'INCLUDE_DIRS',
                                      'LINK_LIBRARIES',
                                      'TYPES_WITH_NAMESPACE',
                                      'MULT_CHAN_TYPES',
                                      'CNV_PFX'])

        if len(args) < 1:
            return self._error ('bad atlas_add_poolcnv_library1: %s' % a)

        ns = kw.get('TYPES_WITH_NAMESPACE', [])
        mult = kw.get('MULT_CHAN_TYPES', [])
        cnv_pfx = kw.get('CNV_PFX', '')
        if type(cnv_pfx) == type([]):
            cnv_pfx = cnv_pfx[0]
        ff = kw.get('FILES', [])
        poolcnv_files = [self._poolcnvns(f, ns, mult) for f in ff]

        extra_libs = _trim_library_list (kw.get('LINK_LIBRARIES', []))
        self.cpppaths.add (kw.get('INCLUDE_DIRS', []))

        d = { 'libname' : args[0],
              'names' : poolcnv_files,
              'cnv_pfx' : cnv_pfx,
              'parse_flags' : '',
              'extra_libs' : extra_libs,
              'srcs' : args[1:] }
        self.poolcnvs.append (d)
        return

    
    def dofun_atlas_add_sercnv_library (self, a):
        self.deps.append ('Trigger/TrigDataAccess/TrigSerializeCnvSvc')

        (args, kw) = self._parse (a, ['FILES',
                                      'INCLUDE_DIRS',
                                      'LINK_LIBRARIES',
                                      'TYPES_WITH_NAMESPACE',
                                      'CNV_PFX'])

        ns = kw.get('TYPES_WITH_NAMESPACE', [])
        cnv_pfx = kw.get('CNV_PFX', '')
        if type(cnv_pfx) == type([]):
            cnv_pfx = cnv_pfx[0]
        ff = kw.get('FILES', [])
        poolcnv_files = [self._poolcnvns(f, ns, []) for f in ff]

        if len(args) < 1 or args[0] != self.packbase + cnv_pfx + 'SerCnv':
            return self._error ('bad atlas_add_sercnv_library1: %s' % a)

        extra_libs = _trim_library_list (kw.get('LINK_LIBRARIES', []))
        self.cpppaths.add (kw.get('INCLUDE_DIRS', []))

        d = { 'names' : poolcnv_files,
              'extra_libs' : extra_libs,
              'srcs' : args[1:],
              'libtag' : cnv_pfx }
        self.sercnvs.append (d)
        return

    
    def dofun_set (self, a):
        if len(a) < 1:
            print ('bad set function1', a, file=sys.stderr)
            self.err = True
            return

        if not isinstance (a[0], ast.Word) or a[0].type != ast.WordType.Variable:
            print ('bad set function2', a, file=sys.stderr)
            self.err = True
            return
        varname = a[0].contents

        ll = [self._getword(aa) for aa in a[1:]]
        try:
            ipos = ll.index('CACHE')
            ll = ll[:ipos]
        except ValueError:
            pass

        #ll = [_stripquotes(l) for l in ll]
        val = self._expand_variables (' '.join(ll))

        self._define (varname, val)
        return


    def _appendarg (self, args, name, val):
        oval = args.get (name, '')
        if oval:
            args[name] = ' '.join ([oval, val])
        else:
            args[name] = val
        return
    
    def dofun_cmake_parse_arguments (self, a):
        if len(a) < 4:
            print ('bad cmake_parse_arguments', a, file=sys.stderr)
            self.err = True
            return

        prefix = self._getword(a[0])
        if prefix == 'PARSE_ARGV':
            print ('unhandled cmake_parse_arguments signature', a, file=sys.stderr)
            self.err = True
            return

        options = self._getstring(a[1]).replace(';',' ').split()
        oneval_kw = self._getstring(a[2]).replace(';',' ').split()
        multval_kw = self._getstring(a[3]).replace(';',' ').split()

        args = [self._expand_variables(self._getword(aa)) for aa in a[4:]]
        argstr = ' '.join(args)
        out = {}
        for o in options:
            out[o] = 'FALSE'
        for o in oneval_kw:
            out[o] = ''
        for o in multval_kw:
            out[o] = ''
        last = None
        for t in ast.tokenize (argstr):
            w = None
            if t.type == ast.TokenType.Word or t.type == ast.TokenType.UnquotedLiteral:
                w = t.content
                if w in options:
                    out[w] = 'TRUE'
                    last = None
                    continue
                elif w in oneval_kw:
                    last = w
                    continue
                elif w in multval_kw:
                    last = [w]
                    continue

            elif t.type == ast.TokenType.QuotedLiteral:
                w = t.content[1:-1]
            else:
                assert 0

            if type(last) == type([]):
                self._appendarg (out, last[0], t.content)
            elif last != None:
                out[last] = t.content
                last = None
            else:
                print ('unparsed argument', w, a, file=sys.stderr)
                self.err = True
                self._appendarg (out, 'UNPARSED_ARGUMENTS', t.content)

        for (k, v) in out.items():
            self._define (prefix + '_' + k, v)
        return


    def dofun_atlas_install_headers (self, a):
        if len(a) != 1 or self._getword(a[0]) != self.packbase:
            print ('unimplemented atlas_install_headers', a, file=sys.stderr)
            self.err = True
            return
        return


    def dofun_add_dependencies (self, a):
        if (len(a) == 2 and
            self._getword(a[0]) == self.packbase + 'Configurables' and
            self._getword(a[1]) == self.packbase + 'PythonInstall'):
            return
        if (len(a) == 2 and
            self._getword(a[0]) == 'PyUtilsInterface' and
            self._getword(a[1]) == self.packbase + 'PythonInstall'):
            return
        if (len(a) == 2 and
            self._getword(a[0]) == self.packbase and
            self._getword(a[1]) == self.packbase + 'DictGen'):
            return
        return self._error ('bad add_dependencies: %s' % a)


    def dofun_message (self, a):
        (args, kw) = self._parse (a, [])
        print ('MESSAGE: ', self._expand_variables (' '.join (args)), file=sys.stderr)
        return


    def dofun_atlas_get_package_name (self, a):
        if len(a) != 1:
            return self._error ('bad atlas_get_package_name: %s', a)
        self._define (self._getword(a[0]), self.packbase)
        return


    def dofun_find_file (self, a):
        (args, kw) = self._parse (a, ['PATH_SUFFIXES',
                                      'PATHS'])
        if len(args) != 2:
            self._error ('bad find_file: %s' % args)
        var = args[0]
        name = _stripquotes(args[1])
        paths = [_stripquotes(self._expand_variables(p)) for p in kw.get('PATHS')]
        suffs = [_stripquotes(self._expand_variables(p)) for p in kw.get('PATH_SUFFIXES')]
        for p in paths:
            for s in suffs:
                fname = os.path.join (p, s, name)
                if os.path.exists (fname):
                    self._define (var, fname)
                    return
            fname = os.path.join (p, name)
            if os.path.exists (fname):
                self._define (var, fname)
                return
        self._error ('Cannot find_file %s %s' % (args, kw))
        return


    def dofun_configure_file (self, a):
        (args, kw) = self._parse (a, ['@ONLY'])
        if len(args) != 2:
            return self._error ('bad configure_file: %s' % a)
        subst_dict = dict ([('@' + k + '@', _stripquotes(v[0])) for k,v in self.variables.items()])
        d = { 'dest' : _stripquotes(args[1]),
              'source' : _stripquotes (args[0]),
              'dict' : subst_dict }
        self.subst_files.append (d)
        return


    def dofun_add_definitions (self, a):
        for aa in a:
            arg = self._expand_variables (self._getword (aa))
            arg = list (shlex.shlex (arg, posix=True, punctuation_chars=True))
            arg = shlex.join (arg)
            self.cppflags.append (arg)
        return


    def dofun_remove_definitions (self, a):
        for aa in a:
            try:
                self.cppflags.remove (self._getword (aa))
            except ValueError:
                pass
        return


    def dofun_string (self, a):
        if (len(a) >= 2 and
            self._getword (a[0]) == 'CONCAT'):
            var = self._getword (a[1])
            s = ''.join ([_stripquotes(self._getword(x)) for x in a[2:]])
            self._define (var, s)
            return
        if (len(a) == 5 and
            self._getword (a[0]) == 'REPLACE' and
            _stripquotes (self._getword (a[2])) == '' and
            _stripquotes (self._getword (a[3])) == 'CMAKE_SHARED_LINKER_FLAGS' and
            _stripquotes (self._getword (a[4])) == '${CMAKE_SHARED_LINKER_FLAGS}' and
            self._getword (a[1]).find('--as-needed') >= 0):
            return

        if (len(a) == 5 and
            self._getword (a[0]) == 'REPLACE' and
            _stripquotes (self._getword (a[2])) == '' and
            _stripquotes (self._getword (a[3])).startswith ('CMAKE_CXX_FLAGS') and
            _stripquotes (self._getword (a[4])).startswith ('${CMAKE_CXX_FLAGS') and
            self._getword (a[1]).find('-Werror=return-type') >= 0):
            return

        if (len(a) == 5 and self._getword (a[0]) == 'REPLACE'):
            match = _stripquotes (self._getword (a[1]))
            repl = _stripquotes (self._getword (a[2]))
            var = _stripquotes (self._getword (a[3]))
            string = _stripquotes (self._expand_variables (self._getword (a[4])))
            self._define (var, string.replace (match, repl))
            return
            

        if (len(a) == 6 and self._getword (a[0]) == 'REGEX' and self._getword (a[1]) == 'REPLACE'):
            match = _stripquotes (self._getword (a[2]))
            repl = _stripquotes (self._getword (a[3]))
            var = _stripquotes (self._getword (a[4]))
            string = _stripquotes (self._expand_variables (self._getword (a[5])))
            repl = repl.replace ('\\\\', '\\')
            newstring = re.sub (match, repl, string)
            self._define (var, newstring)
            return
            

        self._error ('unimplemented string option %s' % self._getword(a[0]))
        return


    def dofun_list (self, a):
        if (len(a) >= 2 and
            self._getword (a[0]) == 'APPEND'):
            var = self._getword (a[1])
            x = self.variables.setdefault (var, ['', False])
            if len (a) > 2:
                for aa in a[2:]:
                    exp = self._expand_variables (_stripquotes (self._getword (aa)))
                    x[0] = x[0] + ' ' + exp
            return

        if (len(a) >= 4 and
            self._getword (a[0]) == 'INSERT'):
            var = self._getword (a[1])
            x = self.variables.setdefault (var, ['', False])
            pos = int (self._getword (a[2]))
            y = self._getword (a[3])
            xl = x[0].split()
            xl.insert (pos, y)
            x[0] = ' '.join (xl)
            return
        return self._error ('unimplemented list call %s' % a)

    
    def dofun_unset (self, a):
        if len(a) != 1:
            return self._error ('unset')
        m = self._getword(a[0])
        if m == 'ROOT_FOUND' : return
        if m in self.macros:
            del self.macros[m]
            return
        if m in self.variables:
            del self.variables[m]
            return
        return self._error ('unset2: ' + m)
        

    def dofun_include_directories (self, a):
        (args, kw) = self._parse (a, [])
        for d in args:
            if d == 'SYSTEM':
                break  # BeamEffects
            if d != 'src':
                self.cpppaths.add (d)
        return


    def dofun_target_compile_definitions (self, a):
        (args, kw) = self._parse (a, ['PRIVATE'])
        if len(args) != 1:
            print ('Unhandled target_compile_definitions1: %s' % a)
            self._err = True
            return

        for d in self.unittests:
            for t in d['tests']:
                if args[0] == self.packbase + '_' + t:
                    if len (d['tests']) != 1:
                        print ('Unhandled target_compile_definitions3: %s' % a)
                        self.err = True
                        return
                    flags = ' '.join ([_stripquotes(x) for x in kw.get('PRIVATE', [])])
                    d['parse_flags'] = d.get('parse_flags', '') + ' ' + flags
                    return

        return self._error ('Unhandled target_compile_definitions2: %s' % a)


    def dofun_target_compile_options (self, a):
        (args, kw) = self._parse (a, ['PRIVATE', 'INTERFACE'])
        if len(args) != 1:
            return self._error ('Unhandled target_compile_options1: %s' % a)
        for d in self.libs:
            if d['libname'] == args[0]:
                flags = ' '.join ([_stripquotes(x) for x in kw.get('PRIVATE', [])])
                d['parse_flags'] = d.get('parse_flags', '') + ' ' + flags
                return
        return self._error ('Unhandled target_compile_options2: %s' % a)


    def dofun_file (self, a):
        (args, kw) = self._parse (a, ['INSTALL', 'DESTINATION', 'MAKE_DIRECTORY', 'GLOB', 'GLOB_RECURSE'])
        recursive = False
        glob_args = kw.get ('GLOB')
        if not glob_args:
            glob_args = kw.get ('GLOB_RECURSE')
            recursive = True
        if glob_args:
            if len (glob_args) < 1:
                return self._error ('Bad file GLOB')
            var = glob_args[0]
            glob_args = glob_args[1:]
            if (len(glob_args) >= 1 and glob_args[0] == 'CONFIGURE_DEPENDS'):
                glob_args = glob_args[1:]
            relative = None
            if len(glob_args) >= 2 and glob_args[0] == 'RELATIVE':
                relative = _stripquotes (glob_args[1])
                glob_args = glob_args[2:]
            out = set()
            for pat in glob_args:
                pat = _stripquotes (pat)
                out.update (glob.glob (pat, recursive=recursive))
                out.update (glob.glob (os.path.join (self.package, pat), recursive=recursive))
            if relative is not None:
                out = [os.path.relpath (p, relative) for p in out]
            out = list(out)
            out.sort()
            self._define (var, ' '.join (out))

        inst = kw.get ('INSTALL')
        dest = kw.get ('DESTINATION')
        d = kw.get ('MAKE_DIRECTORY')
        if d:
            for dd in d:
                self.make_dirs.add (dd)
        if not inst and not dest:
            return
        if not inst or not dest or len(dest) != 1:
            return self._error ('file1')
        inst = ['$PACKAGE/' + _stripquotes(x) for x in inst]
        dest = dest[0]
        self.make_dirs.add (dest)
        for f in inst:
            self.copy_files.append ((f, dest))
        return


    def dofun_get_filename_component (self, a):
        (args, kw) = self._parse (a, [])
        if len(args) < 3:
            self._error ('Bad get_filename_component')
        var = args[0]
        fname = args[1]
        mode = args[2]
        if len(args) >= 4:
            basedir = args[3]
        else:
            basedir = self._expand_variables ('${CMAKE_CURRENT_SOURCE_DIR}')

        if mode == 'DIRECTORY' or mode == 'PATH':
            ret = os.path.dirname(fname)
        elif mode == 'NAME':
            ret = os.path.basename(fname)
        elif mode == 'EXT':
            f = os.path.basename(fname)
            ipos = f.find('.')
            if ipos >= 0:
                ret = f[ipos:]
            else:
                ret = ''
        elif mode == 'NAME_WE':
            ret = os.path.basename(fname)
            ipos = ret.find('.')
            if ipos >= 0:
                ret = ret[:ipos]
        elif mode == 'LAST_EXT':
            ret = os.path.splitext (fname)[1]
        elif mode == 'NAME_WLE':
            f = os.path.basename(fname)
            ret = os.path.splitext (f)[0]
        elif mode == 'ABSOLUTE':
            if os.path.isabs (fname):
                ret = fname
            else:
                ret = os.path.normpath (os.path.join (basedir, fname))
        elif mode == 'REALPATH':
            if os.path.isabs (fname):
                ret = fname
            else:
                ret = os.path.realpath (os.path.join (basedir, fname))
        else:
            self._error ('Unimplemented get_filename_component: ' + str(args))

        self._define (var, ret)
        return


    def dofun_install (self, a):
        # Ignore usage in CxxUtils for now.
        (args, kw) = self._parse (a, ['FILES', 'DESTINATION'])
        flist = [_stripquotes(f) for f in kw.get ('FILES')]
        if flist and flist[0] == 'share/ubsan.supp':
            return
        return self._error ('install')


    def set_test_env (self, test, env):
        if test.startswith (self.packbase + '_'):
            test = test[len(self.packbase)+1:]
        if test.endswith ('_ctest'):
            test = test[:-6]
        for d in self.unittests:
            if test in d['tests']:
                d.setdefault('environment',[]).append (env)
                return
        for d in self.script_tests:
            if test == d['name']:
                d.setdefault('environment',[]).append (env)
                return

        return self._error ('Unhandled set_property ' + test)
    def dofun_set_property (self, a):
        if (len(a) == 6 and
            self._getword(a[0]) == 'TARGET' and
            self._getword(a[2]) == 'APPEND' and
            self._getword(a[3]) == 'PROPERTY' and
            self._getword(a[4]) == 'CXX_CPPCHECK'):
            return

        if (len(a) == 5 and
            self._getword(a[0]) == 'TARGET' and
            self._getword(a[2]) == 'PROPERTY' and
            self._getword(a[3]) == 'POSITION_INDEPENDENT_CODE'):
            return

        if (len(a) == 6 and
            self._getword(a[0]) == 'TEST' and
            self._getword(a[2]) == 'APPEND' and
            self._getword(a[3]) == 'PROPERTY' and
            self._getword(a[4]) == 'ENVIRONMENT'):

            test = self._expand_variables (self._getword (a[1]))
            env = _stripquotes (self._expand_variables (self._getword (a[5])))
            return self.set_test_env (test, env)

        if (len(a) == 5 and
            self._getword(a[0]) == 'TEST' and
            self._getword(a[2]) == 'PROPERTY' and
            self._getword(a[3]) == 'ENVIRONMENT_MODIFICATION'):

            test = self._expand_variables (self._getword (a[1]))
            env = _stripquotes (self._expand_variables (self._getword (a[4])))
            ipos = env.find ('=set:')
            if ipos >= 0:
                env = env[:ipos+1] + env[ipos+5:]
                return self.set_test_env (test, env)

        return self._error ('Unhandled set_property: %s' % a)


    def dofun_link_libraries (self, a):
        (args, kw) = self._parse (a, [])
        self.extra_libs.add (args)
        return


    def dofun_atlas_add_alias (self, a):
        (args, kw) = self._parse (a, [])
        if len(args) != 2:
            return self._error ('Bad add_alias ' + str(a))
        self.aliases.append ({'alias' : args[0],
                              'src' : _stripquotes(args[1])})
        return


    def dofun_atlas_os_id (self, a):
        (args, kw) = self._parse (a, [])
        if len(args) != 2:
            return self._error ('Bad atlas_os_id ' + str(a))
        self._define (args[0], 'Linux')
        self._define (args[1], 'True')
        return


    def dofun_option (self, a):
        (args, kw) = self._parse (a, [])
        if len(args) != 3:
            return self._error ('Bad option ' + str(a))
        self._define (args[0], args[2])
        return

    def dofun_scons (self, args_in):
        args = [self._expand_variables(self._getword(a)) for a in args_in]
        cmd = ' '.join(args)
        self.scons_lines.append (cmd)
        return



    #### There are currently no-ops.
    def dofun_athena_env (self, a):
        return
    def dofun_athena_install_nonstandard_headers (self, a):
        return
    def dofun_cmake_minimum_required (self, a):
        return
    def dofun_atlas_disable_as_needed (self, a):
        return
    def dofun_atlas_install_docs (self, a):
        return
    def dofun_link_directories (self, a):
        return
    def dofun_find_library (self, a):
        return
    def dofun_set_source_files_properties (self, a):
        return
    def dofun_mark_as_advanced (self, a):
        return
    def dofun_get_target_property (self, a):
        return


    def do_macroCall (self, name, args):
        (vlist, body) = self.macros[name]
        args = [self._getword(a) for a in args]
        if len(args) != len(vlist):
            print ('wrong number of arguments to call to macro', name, args, file=sys.stderr)
            self.err = True
            return
        # Fixme: scoping is probably not right, but doesn't matter
        # for any current uses of macros.
        with self._pushvars():
            for (v, a) in zip(vlist, args):
                self._define (v, a)
            self._runbody (body)
        return
    

    def do_userFunctionCall (self, name, args):
        (vlist, body) = self.functions[name]
        args = [self._expand_variables(self._getword(a), quote=True) for a in args]
        args = [_stripquotes(a) if a.find(' ')<0 else a for a in args]
        if len(args) < len(vlist):
            print ('wrong number of arguments to call to function', name, args, file=sys.stderr)
            self.err = True
            return

        with self._pushvars():
            self._define ('ARGC', str(len(args)))
            self._define ('ARGV', ' '.join(args))
            self._define ('ARGN', ' '.join(args[len(vlist):]))
            for i in range(len(vlist)):
                self._define (vlist[i], args[i])
                self._define ('ARGV%d'%i, args[i])
            try:
                self._runbody (body)
            except CMakeReturn:
                pass
        return
    
        
    def do_FunctionCall (self, s):
        fn = getattr (self, 'dofun_' + s.name, None)
        if fn:
            fn (s.arguments)
        elif s.name in self.macros:
            self.do_macroCall (s.name, s.arguments)
        elif s.name in self.functions:
            self.do_userFunctionCall (s.name, s.arguments)
        else:
            print ('Unhandled function:', s, file=sys.stderr)
            self.err = True
        return


    # expr := expr1 [OR expr1 ...]
    # expr1 := expr1a [AND expr1a ...]
    # expr1a := expr2
    #           NOT expr1a
    # expr2 := primary COMP primary
    #          UNARY primary
    #          primary
    # primary := WORD
    #            ( expr )
    _unaryops = ['EXISTS', 'COMMAND', 'DEFINED', 'TARGET', 'POLICY', 'TEST', 'IS_DIRECTORY', 'IS_SYMLINK', 'IS_ABSOLUTE']
    _binaryops = ['EQUAL', 'LESS', 'LESS_EQUAL', 'GREATER', 'GREATER_EQUAL',
                  'STREQUAL', 'STRLESS', 'STRLESS_EQUAL', 'STRGREATER',
                  'STRGREATER_EQUAL', 'VERSION_EQUAL', 'VERSION_LESS',
                  'VERSION_LESS_EQUAL', 'VERSION_GREATER',
                  'VERSION_GREATER_EQUAL', 'MATCHES']
    def _evalunary (self, op, arg):
        if op == 'TARGET':
            if arg == self.packbase:
                return True
            for d in  self.unittests:
                for t in d['tests']:
                    if arg == self.packbase + '_' + t:
                        return True
            # Special case for CLIDComps
            if arg == 'genCLIDDB' or arg == 'CLIDCompsJobOptInstall':
                return False
            return self._error ('Unimplemented TARGET %s' % arg)
        elif op == 'IS_DIRECTORY':
            return os.path.isdir (_stripquotes (self._parse_asstring (arg)))
        return self._error ('Unimplemented unary %s' % op)
    def _evalbinary (self, arg1, op, arg2):
        if op == 'STREQUAL':
            return self._parse_asstring(arg1) == self._parse_asstring(arg2)
        elif op == 'VERSION_EQUAL':
            return _version_compare (self._parse_asstring(arg1), self._parse_asstring(arg2)) == 0
        elif op == 'VERSION_LESS':
            return _version_compare (self._parse_asstring(arg1), self._parse_asstring(arg2)) < 0
        elif op == 'VERSION_LESS_EQUAL':
            return _version_compare (self._parse_asstring(arg1), self._parse_asstring(arg2)) <= 0
        elif op == 'VERSION_GREATER':
            return _version_compare (self._parse_asstring(arg1), self._parse_asstring(arg2)) > 0
        elif op == 'VERSION_GREATER_EQUAL':
            return _version_compare (self._parse_asstring(arg1), self._parse_asstring(arg2)) >= 0
        elif op == 'MATCHES':
            return re.match (self._parse_asstring(arg2), self._parse_asstring(arg1)) != None
        return self._error ('Unimplemented binary %s' % op)
    def _parse_trueconst (self, s):
        if not s: return False
        if s in ['1', 'ON', 'YES', 'TRUE', 'Y']: return True
        if not s[0] in string.digits: return False
        try:
            val = int(s)
            if val: return True
        except (ValueError):
            pass
        return False
    def _parse_falseconst (self, s):
        if s in ['0', 'OFF', 'NO', 'FALSE', 'N', 'IGNORE', 'NOTFOUND', '']: return True
        return s.endswith ('-NOTFOUND')
    def _parse_asbool (self, s):
        if type(s) == type(True):
            return s
        if type(s) == type(''):
            if self._parse_trueconst(s): return True
            if self._parse_falseconst(s): return False
            v = self.variables.get (s, None)
            if v != None:
                if not self._parse_falseconst(v[0]): return True
                return False
        return self._error ('Bad expression1: %s' % s)
    def _parse_asstring (self, s):
        if type(s) == type(''):
            return s
        return str(s)
                
            
                
    def _parseexpr (self, ex, pos):
        ret = self._parseexpr1 (ex, pos)
        while pos[0] < len(ex) and ex[pos[0]] == 'OR':
            pos[0] += 1
            ret2 = self._parseexpr1 (ex, pos)
            ret = self._parse_asbool(ret) or self._parse_asbool(ret2)
        return self._parse_asbool(ret)

    def _parseexpr1 (self, ex, pos):
        ret = self._parseexpr1a (ex, pos)
        while pos[0] < len(ex) and ex[pos[0]] == 'AND':
            pos[0] += 1
            ret2 = self._parseexpr1a (ex, pos)
            ret = self._parse_asbool(ret) and self._parse_asbool(ret2)
        return self._parse_asbool(ret)

    def _parseexpr1a (self, ex, pos):
        if pos[0] < len(ex) and ex[pos[0]] == 'NOT':
            pos[0] += 1
            e = self._parseexpr1a (ex, pos)
            return not self._parse_asbool(e)
        return self._parseexpr2 (ex, pos)
    
    def _parseexpr2 (self, ex, pos):
        if pos[0] < len(ex) and ex[pos[0]] in CMakeReader._unaryops:
            op = ex[pos[0]]
            pos[0] += 1
            prim = self._parseprimary (ex, pos)
            return self._evalunary (op, prim)

        prim = self._parseprimary (ex, pos)
        if pos[0] == len(ex): return prim
        op = ex[pos[0]]
        if not op in self._binaryops:
            return prim
        pos[0] += 1
        prim2 = self._parseprimary (ex, pos)
        return self._evalbinary (prim, op, prim2)

    def _parseprimary (self, ex, pos):
        if pos[0] >= len(ex):
            return self._error ('Bad expression5: %s' % ex)
        if ex[pos[0]] == '(':
            pos[0] += 1
            ret = self._parseexpr (ex, pos)
            if pos[0] >= len(ex) or ex[pos[0]] != ')': 
                return self._error ('Bad expression6: %s' % ex)
            pos[0] += 1
            return ret
        ret = ex[pos[0]]
        pos[0] += 1
        return ret


    def _evalexpr (self, expr):
        ex = self._expand_and_retokenize (expr)
        pos = [0]
        return self._parseexpr (ex, pos)

    
    def _runbody (self, body):
        for s in body:
            if isinstance (s, ast.FunctionCall):
                if s.name == 'return':
                    if s.arguments:
                        return self._error ('bad return: %s' % s)
                    raise CMakeReturn
                self.do_FunctionCall (s)
            elif isinstance (s, ast.IfBlock):
                self.do_IfBlock (s)
            elif isinstance (s, ast.ForeachStatement):
                self.do_ForeachStatement (s)
            elif isinstance (s, ast.MacroDefinition):
                self.do_MacroDefinition (s)
            elif isinstance (s, ast.FunctionDefinition):
                self.do_FunctionDefinition (s)
            else:
                print ('unhandled construct:', s, file=sys.stderr)
                self.err = True
        return


    def _tryif (self, header, body):
        if not isinstance (header, ast.FunctionCall):
            print ('unexpected if construct', file=sys.stderr)
            self.err = True
            return

        if self._evalexpr (header.arguments):
            self._runbody (body)
            return True

        return False


    def do_IfBlock (self, s):
        if self._tryif (s.if_statement.header, s.if_statement.body):
            return
        for ss in s.elseif_statements:
            if self._tryif (ss.header, ss.body):
                return
        if s.else_statement:
            self._runbody (s.else_statement.body)
        return

    
    def do_ForeachStatement (self, s):
        if not isinstance (s.header, ast.FunctionCall) or s.header.name != 'foreach' or len(s.header.arguments) < 2:
            print ('unexpected foreach construct 1', file=sys.stderr)
            self.err = True
            return

        vararg = s.header.arguments[0]
        if not isinstance (vararg, ast.Word) or vararg.type != ast.WordType.Variable:
            print ('unexpected foreach construct 2', file=sys.stderr)
            self.err = True
            return
        varname = vararg.contents

        for vals in self._expand_and_retokenize (s.header.arguments[1:]):
            for val in vals.split(';'):
                self._define (varname, val)
                self._runbody (s.body)
        return


    def do_MacroDefinition (self, s):
        if len(s.header.arguments) < 2:
            print ('not enough arguments in macro definition', file=sys.stderr)
            self.err = True
            return

        mname = self._getword (s.header.arguments[0])
        args = [self._getword(a) for a in s.header.arguments[1:]]

        self.macros[mname] = (args, s.body)
        
        return

    
    def do_FunctionDefinition (self, s):
        if len(s.header.arguments) < 1:
            print ('not enough arguments in function definition', file=sys.stderr)
            self.err = True
            return

        fname = self._getword (s.header.arguments[0])
        args = [self._getword(a) for a in s.header.arguments[1:]]

        self.functions[fname] = (args, s.body)
        
        return


    def translate (self, f, fout):
        self.fout = fout
        t = ast.parse (f.read())

        try:
            self._runbody (t.statements)
        except CMakeReturn:
            pass

        self.dump()
        return


    ####################################################################


    def dump (self):
        # def _genconf_macros (n):
        #     v = self._find_macro ('genconfig_configurable' + n)
        #     if v:
        #         print >> self.fout, "e['GENCONF_%s'] = '%s'" % (n.upper(), v)
        # _genconf_macros ('ModuleName')
        # _genconf_macros ('DefaultName')
        # _genconf_macros ('Algorithm')
        # _genconf_macros ('AlgTool')
        # _genconf_macros ('Auditor')
        # _genconf_macros ('Service')

        if self.args.get('DeferTests'):
            print ("e['DEFER_TESTS'] = True", file=self.fout)

        ll = list(self.cpppaths)
        ll.sort()
        for p in ll:
            print ("e.Append(CPPPATH = '%s')"%p, file=self.fout)
            if not p.startswith('/') and not p.startswith('$'):
                print ("e.Append(CPPPATH = '$PACKAGE/%s')"%p, file=self.fout)

        ll = list(self.public_cpppaths)
        ll.sort()
        for p in ll:
            print ("e.Append(PUBLIC_CPPFLAGS = ' -I%s')"%p, file=self.fout)

        ll = list(self.extra_libs)
        ll.sort()
        for p in ll:
            print ("e.Append(BASELIBS = ' %s')"%p, file=self.fout)
        # flags = self._find_macro ('use_pp_cppflags')
        # if flags:
        #     print ("e.Append(PUBLIC_CPPFLAGS = ' %s ')"%flags, file=self.fout)
        for p in self.cppflags:
            print ("e.Append(CPPFLAGS = ' ' + %s)"%repr(p), file=self.fout)
        for p in self.public_cppflags:
            print ("e.Append(PUBLIC_CPPFLAGS = ' ' + %s)"%repr(p), file=self.fout)
        # for p in self.cxxflags:
        #     print ("e.Append(CXXFLAGS = ' %s')"%p, file=self.fout)
        for p in self.libpaths:
            print ("e.Append(PACKAGE_LIBPATH = '%s')"%p, file=self.fout)
        for p in self.deps:
            print ("add_deps ('" + p + "')", file=self.fout)
        for p in self.private_deps:
            print ("add_private_deps ('" + p + "')", file=self.fout)
        if self.make_dirs or self.copy_files:
            print ("import SCons.Action", file=self.fout)
            print ("save_print = SCons.Action.print_actions", file=self.fout)
            print ("SCons.Action.print_actions = 0", file=self.fout)
            dirs = list (self.make_dirs)
            dirs.sort()
            for dest in dirs:
                if (not dest.startswith ('$BUILDDIR/') and
                    not dest.startswith ('$BUILDNAME/')):
                    self._error ('Mkdir outside $BUILDDIR:' + dest)
                    continue
                print ("Execute(Mkdir(e.subst('%s')))" % dest, file=self.fout)
            for (f, dest) in self.copy_files:
                print ("Execute(Copy(e.subst('%s'), e.subst('%s')))" % (os.path.join (dest, os.path.basename (f)), f), file=self.fout)
            print ("SCons.Action.print_actions = save_print", file=self.fout)
        for p in self.install_jo:
            self.print_call ('install_jo', p)
        for p in self.install_python:
            self.print_call ('install_python', p)
        # for p in self.install_data:
        #     print ("install_data (" + _maybequote(p) + ")", file=self.fout)
        for p in self.install_share:
            print ("install_share (" + _maybequote(p) + ")", file=self.fout)
        for p in self.install_xml:
            print ("install_xml (" + _maybequote(p) + ")", file=self.fout)
        for p in self.install_files:
            self.print_call ('install_files', p)
        for p in self.subst_files:
            self.print_call ('subst_file', p)

        # extra_srcs = {}
        for (headers, linkdef, lib, rootcint) in self.root_headers:
            print (lib + '_cint=', file=self.fout, end=' ')
            libs = []
            for ll in self.libdefs:
                if ll['libname'] == lib:
                    libs = ll['public_libs'] + ll['private_libs']
                    break
            if lib.endswith ('Lib'): lib = lib[:-3]
            libs.append(lib)
            for ll in self.libs:
                if ll['libname'] == lib or ll['libname'] == lib + '_components':
                    libs += ll['extra_libs']
            self.print_call ('build_rootcint',
                             {'headers': headers,
                              'rootcint_name' : rootcint,
                              'libs' : libs,
                              'linkdef_in': linkdef})

        for d in self.gen_reflex:
            self.print_call ('build_reflex', d)

        include_current = self._parse_trueconst (self._expand_variables ('${CMAKE_INCLUDE_CURRENT_DIR}'))
        libnames = set()
        for d in self.libdefs:
            if include_current: d.setdefault ('private_includes', []).append ('$BUILDDIR')
            d['libname'] = _trimlib (d['libname'])
            d['public_libs'] = [_trimlib(l) for l in d['public_libs']]
            d['private_libs'] = [_trimlib(l) for l in d['private_libs']]
            self.print_call ('build_libdef', d)
            libnames.add (d['libname'])

        autouic = self._parse_trueconst (self._expand_variables ('${CMAKE_AUTOUIC}'))
        for d in self.libs:
            if autouic: d['autouic'] = True
            d['libname'] = _trimlib (d.get ('libname', self.packbase))
            if d['libname'].endswith ('_components'):
                basename = d['libname'][:-11]
                if basename not in libnames:
                    d['libname'] = basename
            self.print_call ('build_lib', d)

        for d in self.xaod_dicts:
            self.print_call ('build_xaod_selection', d)

        for d in self.dicts:
            self.print_call ('build_dict', d)

        for d in self.poolcnvs:
            self.print_call ('build_poolcnv', d)

        for d in self.sercnvs:
            self.print_call ('build_sercnv', d)

        for d in self.bins:
            self.print_call ('build_bin', d)
        if self.scripts:
            print ("install_scripts ('" + ' '.join (self.scripts) + "')", file=self.fout)
        for d in self.aliases:
            self.print_call ('make_alias', d)

        extradb = self.args.get ('extradb')
        for d in self.unittests:
            if extradb: d['extradb'] = extradb
            self.print_call ('build_tests', d)
        for d in self.script_tests:
            if extradb: d['extradb'] = extradb
            self.print_call ('script_test', d)
        for d1, deps in self.depends:
            for d2 in deps:
                print ("e.Depends ('%s', '%s')" % (d1, d2), file=self.fout)

        for l in self.scons_lines:
            print (l, file=self.fout)

        return


    def print_call (self, fn, d):
        lead = '%s (' % fn
        indent = ''
        print (lead, file=self.fout, end=' ')
        keys = list(d.keys())
        keys.sort()
        for k in keys:
            v = d[k]
            print ('%s%s = %s,' % (indent, k, repr(v)), file=self.fout, end=' ')
            indent = '\n' + ' ' * len (lead)
        print (')', file=self.fout)
        return


if __name__ == "__main__":
    import sys
    infile = sys.argv[1]
    pack = os.path.dirname(infile)
    if not pack:
        pack=None
    elif pack == 'cmaketests':
        pack = os.path.basename(infile)
    r = CMakeReader(pack)
    r.translate (open(infile), sys.stdout)
    if r.err: print ('ERROR')
    if pack:
        if os.path.exists (os.path.join ('SConsFiles', os.path.basename (pack))):
            print ('OVERRIDDEN PACKAGE!')

#python cmakeast/printer.py CMakeLists.txt 

